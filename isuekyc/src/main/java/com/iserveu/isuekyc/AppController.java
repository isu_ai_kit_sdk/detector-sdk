package com.iserveu.isuekyc;

import android.app.Application;

import co.hyperverge.hypersnapsdk.HyperSnapSDK;
import co.hyperverge.hypersnapsdk.objects.HyperSnapParams;

public class AppController extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        HyperSnapSDK.init(this, "45ac9b", "7143ad80089cd1dfba9e", HyperSnapParams.Region.India);

    }
}
