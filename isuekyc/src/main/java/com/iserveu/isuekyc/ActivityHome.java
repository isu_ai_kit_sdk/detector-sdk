package com.iserveu.isuekyc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontModelISUeKyc;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.databinding.ActivityHomeBinding;
import com.iserveu.isuekyc.intent.IntentFactory;
import com.iserveu.isuekyc.utils.EKycConsentAgreementActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

public class ActivityHome extends AppCompatActivity {

    private static final String TAG = ActivityHome.class.getSimpleName();
    private ActivityHomeBinding binding;
    private String encodedImage ="";
    private String encodedSignature = "";
    private String encodedUserImageAadhaar ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.openButton.setOnClickListener(view -> {
            String key = binding.keyText.getText().toString();
            if (key.equalsIgnoreCase("aadharfront")){
                Intent intent = IntentFactory.returnAadhaarFrontCameraActivity(ActivityHome.this);
                startActivity(intent);
            }else if (key.equalsIgnoreCase("aadharback")){
                Intent intent = IntentFactory.returnAadhaarBackCameraActivity(ActivityHome.this);
                startActivity(intent);
            }else if (key.equalsIgnoreCase("pancard")){
                Intent intent = IntentFactory.returnPanCameraActivity(ActivityHome.this);
                startActivity(intent);
            }else if (key.equalsIgnoreCase("faceliveness")){
                Intent intent = IntentFactory.returnLivenessFaceDetectionActivity(ActivityHome.this);
                startActivity(intent);
            }else if (key.equalsIgnoreCase("consent")){
                Intent intent = IntentFactory.returnEKycConsentAgreementActivity(ActivityHome.this);
                startActivity(intent);
            }else if (key.equalsIgnoreCase("facematch")){
                if (BitmapHolder.getInstance().getAadhaarFaceImageEncoded()!=null){
                    Intent intent = IntentFactory.returnFaceMatchActivity(ActivityHome.this);
                    startActivity(intent);
                }else{
                    Toast.makeText(this, "Capture aadhar image first ", Toast.LENGTH_SHORT).show();
                }

            }else{
                Toast.makeText(this, "Invalid key", Toast.LENGTH_SHORT).show();
            }

        });

        /*binding.keyText.setOnClickListener(view -> {
            if (binding.keyText.hasFocus()){
                binding.responseTextView.setText("");
            }
        });*/

        binding.matchImage.setOnClickListener(view -> {
//           addFaceToModel()
            startActivity(IntentFactory.returnFaceDetectionActivity(this));
        });

        binding.keyText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (view.isFocused()){
                    binding.responseTextView.setText("");
                    binding.userImageIV.setImageBitmap(null);
                    binding.userSignatureIV.setImageBitmap(null);
                    binding.matchImage.setVisibility(View.GONE);
                    BitmapHolder.getInstance().clearData();

                }
            }
        });

    }

    private String encodeUserImage(String s) {

        Bitmap bmp = null;
        try {
            FileInputStream is = this.openFileInput(s);
            bmp = BitmapFactory.decodeStream(is);

            is.close();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            return Base64.encodeToString(byteArray, Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
            return "n/a";
        }

    }
    private Bitmap getSavedBitmap(String s) {

        Bitmap bmp = null;
        try {
            FileInputStream is = this.openFileInput(s);
            bmp = BitmapFactory.decodeStream(is);

            is.close();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            /*byte[] byteArray = byteArrayOutputStream .toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);*/
           return bmp;
        } catch (Exception e) {
            e.printStackTrace();
           return bmp;
        }

    }

    private String encodeUserSignature(String s) {

        Bitmap bmp = null;
        try {
            FileInputStream is = this.openFileInput(s);
            bmp = BitmapFactory.decodeStream(is);

            is.close();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
             return Base64.encodeToString(byteArray, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return "n/a";
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    protected void onResume() {
        super.onResume();

        binding.keyText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (view.isFocused()){
                    binding.responseTextView.setText("");
                    binding.userImageIV.setImageBitmap(null);
                    binding.userSignatureIV.setImageBitmap(null);
                    binding.matchImage.setVisibility(View.GONE);
                }
            }
        });
        if (getIntent().hasExtra("imgType")){
            String imgType = getIntent().getStringExtra("imgType");
            JSONObject jsonObject = new JSONObject();



            if (imgType.equalsIgnoreCase("aadharFront")){
               /* String name = getIntent().getStringExtra("name");
                String dob = getIntent().getStringExtra("DOB");
                String gender = getIntent().getStringExtra("gender");
                String aadharNo = getIntent().getStringExtra("aadhar");
                String vid = getIntent().getStringExtra("VID");
                boolean isUserImageSaved = getIntent().getBooleanExtra("isUserImageSaved",false);
                Log.d(TAG, "onCreate: isUserImageSaved boolean :: "+isUserImageSaved);*/

                String name = AadhaarFrontModelISUeKyc.getInstance().getAadhaarName();
                String dob = AadhaarFrontModelISUeKyc.getInstance().getAadhaarDOB();
                String gender = AadhaarFrontModelISUeKyc.getInstance().getAadhaarGender();
                String aadharNo = AadhaarFrontModelISUeKyc.getInstance().getAadhaarNumber();
                String vid = AadhaarFrontModelISUeKyc.getInstance().getAadhaarVIDNumber();
                boolean isUserImageSaved = true;
                Log.d(TAG, "onCreate: isUserImageSaved boolean :: "+isUserImageSaved);



                        if (isUserImageSaved){
//                            encodedUserImageAadhaar =  encodeUserImage("detected_user_image_aadhaar.png");
                            String encImage =  BitmapHolder.getInstance().getAadhaarFaceImageEncoded();
                            if (encImage!=null){
//                                Bitmap imgBitmap = decodeBitmap(encImage);
                                Bitmap imgBitmap = decodeBitmap(AadhaarFrontModelISUeKyc.getInstance().getAadhaarEncodedUserImage());
                                binding.userImageIV.setImageBitmap(imgBitmap);
                                Log.d(TAG, "onCreate: encodedUserImageAadhaar :: "+encodedUserImageAadhaar);
                            }else{
                                Toast.makeText(this, "getting error", Toast.LENGTH_SHORT).show();
                            }
                            try {


                                        jsonObject.put("name", name);
                                        jsonObject.put("dob",dob);
                                        jsonObject.put("gender",gender);
                                        jsonObject.put("aadharNo",aadharNo);
                                        jsonObject.put("vid",vid);
                                        jsonObject.put("isUserImageSaved",isUserImageSaved);
//                                        jsonObject.put("encodedUserImage",encodedUserImageAadhaar);
//                                        Toast.makeText(this, ""+encodedUserImageAadhaar, Toast.LENGTH_SHORT).show();

                                        binding.responseTextView.setText(jsonObject.toString());
//                                        binding.responseTextView.append(encodedUserImageAadhaar);
                                        Log.d(TAG, "run: aadharFrontResponseJSON "+jsonObject.toString());

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Log.d(TAG, "onCreate: encodedUserImageAadhaar :: "+e.getLocalizedMessage());
                                    }


                        }else{
                            try {
                                jsonObject.put("name",name);
                                jsonObject.put("dob",dob);
                                jsonObject.put("gender",gender);
                                jsonObject.put("aadharNo",aadharNo);
                                jsonObject.put("vid",vid);
                                jsonObject.put("isUserImageSaved",isUserImageSaved);
                                jsonObject.put("encodedUserImage",encodedUserImageAadhaar);

                                Log.d(TAG, "run: aadharFrontResponseJSON "+jsonObject.toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.d(TAG, "onCreate: encodedUserImageAadhaar :: "+e.getLocalizedMessage());
                            }
                        }

//                binding.responseTextView.setText(jsonObject.toString());


            }
            if (imgType.equalsIgnoreCase("aadhar_face_and_selfie_face")){
                showCapturedImage();
                showLiveImage();
            }

            if (imgType.equalsIgnoreCase("aadharBack")){
                String address = getIntent().getStringExtra("address");
                String state = getIntent().getStringExtra("state");
                String pin = getIntent().getStringExtra("pincode");
                String aadharNo = getIntent().getStringExtra("aadhar");
                String vid = getIntent().getStringExtra("VID");

                try {
                    jsonObject.put("address",address);
                    jsonObject.put("state",state);
                    jsonObject.put("pin",pin);
                    jsonObject.put("aadharNo",aadharNo);
                    jsonObject.put("vid",vid);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                binding.responseTextView.setText(jsonObject.toString());

            }

            if (imgType.equalsIgnoreCase("liveness_image")){
                String encImage =  BitmapHolder.getInstance().getSetLiveNessImageEncoded();
                if (encImage!=null){
                    Bitmap imgBitmap = decodeBitmap(encImage);
                    binding.userImageIV.setImageBitmap(imgBitmap);
                    Log.d(TAG, "onCreate: encodedUserImageAadhaar :: "+encodedUserImageAadhaar);
                }else{
                    Toast.makeText(this, "getting error", Toast.LENGTH_SHORT).show();
                }
            }


            if (imgType.equalsIgnoreCase("panCard")){
                String  panName= getIntent().getStringExtra("panName");
                String panDOB = getIntent().getStringExtra("panDOB");
                String panFatherName = getIntent().getStringExtra("panFatherName");
                String panNo = getIntent().getStringExtra("panNo");
                String panMotherName = getIntent().getStringExtra("panMotherName");
                String encodedUserSignature = getIntent().getStringExtra("encodedUserSignature");
                String encodedUserImage = getIntent().getStringExtra("encodedUserImage");
                boolean isUserImageSaved = getIntent().getBooleanExtra("isUserImageSaved",false);
                boolean isUserSignatureSaved = getIntent().getBooleanExtra("isUserSignatureSaved",false);
                boolean isMotherName = getIntent().getBooleanExtra("isMotherName",false);


                if (isUserImageSaved){
                    encodedUserImage = encodeUserImage("detected_bitmap.png");
                    Bitmap imgBitmap = decodeBitmap(encodedUserImage);
                    binding.userImageIV.setImageBitmap(imgBitmap);
                    Log.d(TAG, "onResume: encodedImage :: "+encodedImage);

                }
                if (isUserSignatureSaved){

                    encodedUserSignature = encodeUserSignature("detected_signature.png");
                    Bitmap imgBitmap = decodeBitmap(encodedUserSignature);
                    binding.userSignatureIV.setImageBitmap(imgBitmap);
                    Log.d(TAG, "onResume: encodedSignature :: "+encodedSignature);

                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            jsonObject.put("panName",panName);
                            jsonObject.put("panFatherName",panFatherName);
                            jsonObject.put("isMotherName",isMotherName);
                            jsonObject.put("panMotherName",panMotherName);
                            jsonObject.put("panDOB",panDOB);
                            jsonObject.put("panNo",panNo);
                            jsonObject.put("isUserImageSaved",isUserImageSaved);
                            jsonObject.put("isUserSignatureSaved",isUserSignatureSaved);
                           /* jsonObject.put("encodedSignature",encodedSignature);
                            jsonObject.put("encodedUserImage",encodedImage);*/
                            binding.responseTextView.setText(jsonObject.toString());


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },1000);

            }

        }
    }

    private void showLiveImage() {
        try {
//            Bitmap bitmap = decodeBitmap(BitmapHolder.getInstance().getLiveFaceImageEncoded());
            Bitmap bitmap = BitmapHolder.getInstance().getLiveImageBitmap();
            binding.userSignatureIV.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "showLiveImage: "+e.getLocalizedMessage());
        }
    }

    private void showCapturedImage() {
        try {

            Bitmap bitmap = decodeBitmap(BitmapHolder.getInstance().getAadhaarFrontImageEncoded());
            binding.userImageIV.setImageBitmap(bitmap);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "showCapturedImage: "+e.getLocalizedMessage());

        }

    }

    private Bitmap decodeBitmap(String encodedImage) {

        byte[] decodedBytes = Base64.decode(encodedImage, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }
}