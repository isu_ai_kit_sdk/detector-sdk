package com.iserveu.isuekyc.liveness;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.core.ResolutionInfo;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceContour;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.google.mlkit.vision.face.FaceLandmark;
import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontCameraActivity;
import com.iserveu.isuekyc.dialogs.DialogFactory;
import com.iserveu.isuekyc.intent.IntentFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class FaceDetectionActivity extends AppCompatActivity {

    private static final String TAG = FaceDetectionActivity.class.getSimpleName();
    private Preview preview;
    private PreviewView previewView,previewBorder;
    private Camera camera;
    private RelativeLayout view;
    private FloatingActionButton button;
    private ImageView cameraControllerBtn;
    private ImageView capturedImageView;
    private ImageView imgDone;
    private ImageView imgCancel;
    private Bitmap bitmap;
    private TextView faceStatusTv,taskTextView,timerTextView;
    public static boolean isTimerActive = false;
    private CameraListener cameraListener;
    private String MODEL_FILE = "model.tflite";
    private ProgressDialog progressDialog;
    public static boolean isCaptureBtnClicked = false;
    private Executor executor = Executors.newSingleThreadExecutor();
    private  ImageCapture imageCapture;
    private ImageView closeCameraButton;
    private Button proceedButton;
    private RelativeLayout selfieConsentLayout;
    private ConstraintLayout livenessSelfieLayout;
    private TextView captureText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liveness_activity);
        initViews();
        renderImage();
        try {
            loadMaskDetectionModel(this,MODEL_FILE);
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "onCreate: Error while loading model "+e.getLocalizedMessage());
        }

    }

    private MappedByteBuffer loadMaskDetectionModel(Activity activity, String MODEL_FILE) throws IOException {
        AssetFileDescriptor fileDescriptor = activity.getAssets().openFd(MODEL_FILE);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);

    }

    private void initViews() {
        if (ContextCompat.checkSelfPermission(FaceDetectionActivity.this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(FaceDetectionActivity.this, new String[] {Manifest.permission.CAMERA}, 101);
        }

        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Capturing image...");
        progressDialog.setCancelable(false);

        previewView = findViewById(R.id.pv);
        previewBorder = findViewById(R.id.pv_border);
        view = findViewById(R.id.conlay);
        faceStatusTv = findViewById(R.id.textView3);
        taskTextView = findViewById(R.id.task_Tv);
        taskTextView.setTag("");
        timerTextView = findViewById(R.id.timer_textView);
        cameraControllerBtn = findViewById(R.id.camera_controller_btn);
        closeCameraButton = findViewById(R.id.close_liveness_camera);
        proceedButton = findViewById(R.id.proceed_liveness_selfie_capture_btn);
        selfieConsentLayout = findViewById(R.id.selfie_consent_layout_liveness);
        livenessSelfieLayout = findViewById(R.id.liveness_selfie_camera_layout);
        captureText = findViewById(R.id.capture_text_liveness);

        Button checkFaceMaskButton = findViewById(R.id.faceMaskCheckBtn);



        imgCancel = findViewById(R.id.imgCancel);
        capturedImageView = findViewById(R.id.imgCapture);
        imgDone = findViewById(R.id.imgDone);

        selfieConsentLayout.setVisibility(View.VISIBLE);
        livenessSelfieLayout.setVisibility(View.GONE);

        proceedButton.setOnClickListener(view1 -> {
            selfieConsentLayout.setVisibility(View.GONE);
            livenessSelfieLayout.setVisibility(View.VISIBLE);
        });

        closeCameraButton.setOnClickListener(view1 -> {
            finishAffinity();
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideImagePreview();
            }
        });

        cameraControllerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                isCaptureBtnClicked = true;
                progressDialog.show();
            }
        });

        capturedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bitmap != null){
                    Intent intent = new Intent(FaceDetectionActivity.this, FullImageView.class);
                    startActivity(intent);
                }
            }
        });
        imgDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    isCaptureBtnClicked = true;
                    progressDialog.show();

            }
        });


    }

    private void hideImagePreview() {
        capturedImageView.setVisibility(View.GONE);
        imgCancel.setVisibility(View.GONE);
        imgDone.setVisibility(View.GONE);
        cameraControllerBtn.setVisibility(View.VISIBLE);
    }


    private void renderImage() {
        Log.d(TAG, "renderImage: called");
        Log.d(TAG, "renderImage: isTimerActive "+isTimerActive);

        ListenableFuture<ProcessCameraProvider> val = ProcessCameraProvider.getInstance(this);

        val.addListener(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.P)
            @Override
            public void run() {
                Log.d(TAG, "renderImage run: called");
                try {
                    ProcessCameraProvider processCameraProvider= val.get();
                    preview = new Preview.Builder().setTargetResolution(new Size(900,1200)).build();

                    CameraSelector cameraSelector = new CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_FRONT).build();

                    preview.setSurfaceProvider(previewView.getSurfaceProvider());


                    imageCapture = new ImageCapture.Builder().setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY).build();
                    ImageAnalysis imageAnalysis1 = new ImageAnalysis.Builder().setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                            .setOutputImageRotationEnabled(true)
                            .setTargetResolution(new Size(900,1200)).build();

                    imageAnalysis1.setAnalyzer(ContextCompat.getMainExecutor(FaceDetectionActivity.this)
                            ,new ImageAnalyzer(FaceDetectionActivity.this, previewView, faceStatusTv, taskTextView, timerTextView, capturedImageView, true, new CameraListener() {
                                @Override
                                public void getCameraStatus(boolean shouldStopCamera) {

                                }
                            }, new MovementListener() {
                                @Override
                                public void isMovementDetected(boolean isDetected) {
                                   if (isDetected){
                                       previewBorder.setBackground(getResources().getDrawable(R.drawable.green_preview_view_border));
                                       imgDone.setVisibility(View.VISIBLE);
                                       captureText.setText("Capture Now");

                                   }else{
                                       previewBorder.setBackground(getResources().getDrawable(R.drawable.red_preview_view_border));
                                       imgDone.setVisibility(View.GONE);
                                       captureText.setText("Place your face within circle");
                                   }
                                }
                            }));

                        processCameraProvider.unbindAll();
                        camera = processCameraProvider.bindToLifecycle(FaceDetectionActivity.this,cameraSelector,preview,imageAnalysis1);
                } catch (ExecutionException e) {
                    Log.d(TAG, "run: "+e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    Log.d(TAG, "run: "+e.getLocalizedMessage());
                    e.printStackTrace();
                }

            }
        }, ContextCompat.getMainExecutor(FaceDetectionActivity.this));

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (progressDialog!=null){
            progressDialog.dismiss();
        }
    }
}