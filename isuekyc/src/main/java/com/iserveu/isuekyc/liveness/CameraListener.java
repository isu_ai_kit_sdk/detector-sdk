package com.iserveu.isuekyc.liveness;

public interface CameraListener {
    void getCameraStatus(boolean isCameraOn);
}
