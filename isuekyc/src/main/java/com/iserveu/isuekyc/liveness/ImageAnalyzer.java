package com.iserveu.isuekyc.liveness;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;

import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.YuvImage;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.media.Image;

import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Trace;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageInfo;
import androidx.camera.core.ImageProxy;
import androidx.camera.view.PreviewView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.common.internal.ImageConvertUtils;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceContour;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.google.mlkit.vision.face.FaceLandmark;
import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.dialogs.DialogFactory;
import com.iserveu.isuekyc.intent.IntentFactory;

import org.tensorflow.lite.Interpreter;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.ReadOnlyBufferException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ImageAnalyzer implements ImageAnalysis.Analyzer {

    private static final String TAG = ImageAnalyzer.class.getSimpleName();
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private Executor executor = Executors.newSingleThreadExecutor();




    //    FirebaseVisionFaceDetector firebaseVisionFaceDetector;

    Context context;
    private PreviewView rootview;
    private TextView textView,taskTextView,timerTextView;
    private ImageView capturedImgView;
    private int eyeBlinkCount =0;
    private Bitmap bitmap;
    private boolean isTimerActive;
    private CameraListener cameraListener;
    private MovementListener movementListener;
    private InputImage image;
    private CountDownTimer countDownTimer;
    private boolean isCaptureEnabled = false;
    private int rotationCompensation;
    private CameraManager cameraManager;


    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }



    public ImageAnalyzer(Context context, PreviewView view, TextView textView,TextView taskTextView,TextView timerTextView, ImageView mCapturedImgView,boolean isTimerActive,CameraListener mCameraListener,MovementListener movementListener) {

        this.context = context;
        this.rootview = view;
        this.textView = textView;
        this.capturedImgView = mCapturedImgView;
        this.taskTextView = taskTextView;
        this.isTimerActive = isTimerActive;
        this.cameraListener = mCameraListener;
        this.timerTextView = timerTextView;
        this.movementListener = movementListener;
        cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);


    }

    public ImageAnalyzer(Context mContext,Bitmap mBitmap){

    }

    @Override
    public void analyze(@NonNull ImageProxy imageProxy) {
        Log.d(TAG, "analyze: called");
        @SuppressLint("UnsafeOptInUsageError")
        Image mediaImage = imageProxy.getImage();
        ImageInfo imageInfo = imageProxy.getImageInfo();
        int rotDegree=imageProxy.getImageInfo().getRotationDegrees();
        try {
            String cameraId = getFrontFacingCameraId(cameraManager);
             rotationCompensation = getRotationCompensation(cameraId,(Activity) context,true,rotDegree);
        } catch (CameraAccessException e) {
            Log.d(TAG, "analyze: unable to get cameraId ");
        }

        ImageCapture.Metadata metadata = new ImageCapture.Metadata();
        metadata.setReversedHorizontal(true);

        if (mediaImage != null) {


            image = InputImage.fromMediaImage(mediaImage, rotDegree);

            FaceDetectorOptions options =
                    new FaceDetectorOptions.Builder()
                            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
//                            .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                            .setContourMode(FaceDetectorOptions.CONTOUR_MODE_ALL)
                            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                            .setMinFaceSize(0.15f)
                            .enableTracking()
                            .build();


            // Real-time contour detection
            FaceDetectorOptions realTimeOpts =
                    new FaceDetectorOptions.Builder()
                            .setContourMode(FaceDetectorOptions.CONTOUR_MODE_ALL)
                            .build();



            FaceDetector detector = FaceDetection.getClient(options);
            FaceDetector detector1 = FaceDetection.getClient(realTimeOpts);
            detector.process(image).addOnSuccessListener(new OnSuccessListener<List<Face>>() {
                @Override
                public void onSuccess(List<Face> faces) {

                        processFaces(faces,mediaImage);

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "analyze onFailure: "+e.getLocalizedMessage());
                }
            }).addOnCompleteListener(new OnCompleteListener<List<Face>>() {
                @Override
                public void onComplete(@NonNull Task<List<Face>> task) {
                    imageProxy.close();
                }
            });

        }

    }

    /*private Bitmap toBitmap(@NonNull ImageProxy image) {
        if(bitmapBuffer == null){
            bitmapBuffer = Bitmap.createBitmap(image.getWidth(),image.getHeight(),Bitmap.Config.ARGB_8888);
        }
        bitmapBuffer.copyPixelsFromBuffer(image.getPlanes()[0].getBuffer());
        return bitmapBuffer;
    }*/


    String getFrontFacingCameraId(CameraManager cManager){
        try {
            for(final String cameraId : cManager.getCameraIdList()){
                CameraCharacteristics characteristics = cManager.getCameraCharacteristics(cameraId);
                int cOrientation = characteristics.get(CameraCharacteristics.LENS_FACING);
                if(cOrientation == CameraCharacteristics.LENS_FACING_FRONT) return cameraId;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "getFrontFacingCameraId: unable to get camera Id");
        }
        return null;
    }

    private void getImageFromInputImage(){
        if (image!=null){
            try {
                Log.d(TAG, "analyze: getting bitmap");
                bitmap = ImageConvertUtils.getInstance().getUpRightBitmap(image);
                captureImage();
            } catch (MlKitException e) {
                Log.d(TAG, "analyze: can't get bitmap");
                e.printStackTrace();
            }
        }
    }
    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);
        return b;
    }

    private boolean checkFaceRotation(Face face) {
        boolean isDetected= false;
        if (face!=null) {
            float rotX = face.getHeadEulerAngleX();  // Head is rotated to the left rotX degrees
            float rotY = face.getHeadEulerAngleY();  // Head is rotated to the right rotY degrees
            float rotZ = face.getHeadEulerAngleZ();  // Head is tilted sideways rotZ degrees

            Log.d(TAG, "checkFaceRotation: " +rotX+" "+rotY +" "+rotZ);
            if (rotX>10){
                Log.d(TAG, "checkFaceRotation: rotated to top" );
//                Toast.makeText(context, "face towards top", Toast.LENGTH_SHORT).show();
                isDetected = true;
            }else{
                Log.d(TAG, "checkFaceRotation: face is straight");

            }

            if (rotY>10){
                Log.d(TAG, "checkFaceRotation: rotated to  left" );
//                textView.setText(" face towards left");
                isDetected = true;
            }else{
                Log.d(TAG, "checkFaceRotation: face is straight");

            }
            if (rotX<-10){
                Log.d(TAG, "checkFaceRotation: rotated to bottom" );
//                Toast.makeText(context, "face towards bottom", Toast.LENGTH_SHORT).show();
                isDetected = true;

            }else{
                Log.d(TAG, "checkFaceRotation: face is straight");

            }

            if (rotY<-10){
                Log.d(TAG, "checkFaceRotation: rotated to right");
                textView.setText(" face towards right");
                isDetected = true;
            }else{
                Log.d(TAG, "checkFaceRotation: face is straight");

            }


        }
        return isDetected;
    }


    private void captureImage() {
        Log.d(TAG, "captureImage: called");


        if (bitmap != null){
            Log.d(TAG, "onFinish: bitmap is not null");
                 /*   capturedImgView.setImageBitmap(bitmap);
                    capturedImgView.setVisibility(View.VISIBLE);*/
            createImageFromBitmap(bitmap);
            String encodedB = encodeUserImage(bitmap);
            FaceLivenessModelISUeKyc.getInstance().setFaceLivenessImageEncoded(encodedB);
            eyeBlinkCount=0;
//            taskTextView.setText("Image captured");
            FaceDetectionActivity.isCaptureBtnClicked = false;
            goToFullImageActivity();
        }else{
            Log.d(TAG, "onFinish: bitmap is null");
        }
    }

    private String encodeUserImage(Bitmap bmp) {

        try {

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
            BitmapHolder.getInstance().setLiveFaceImageEncoded(encoded);
            return encoded;
        } catch (Exception e) {
            e.printStackTrace();
            return "n/a";
        }

    }


    private void goToFullImageActivity() {
        Intent intent = new Intent(IntentFactory.returnAppMainActivity(context)).putExtra("imgType","liveness_image");
        context.startActivity(intent);
    }

    private Bitmap generateDynamicBitmap(View view){
        View dynamicView = rootview;
        dynamicView.setDrawingCacheEnabled(true);
        dynamicView.setBackgroundColor(context.getResources().getColor(R.color.white));
        Bitmap bitmap = Bitmap.createBitmap(dynamicView.getDrawingCache());
        dynamicView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    private void showImagePreview(Bitmap bitmap) {
        capturedImgView.setVisibility(View.VISIBLE);
        try {
            bitmap = BitmapFactory.decodeStream(context.openFileInput("myImage"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        capturedImgView.setImageBitmap(bitmap);
    }

    public String createImageFromBitmap(Bitmap bitmap) {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }

    private static void logExtrasForTesting(Face face) {
        if (face != null) {
            if (face.getSmilingProbability() != null) {
                float smileProb = face.getSmilingProbability();
            }

            // All landmarks
            int[] landMarkTypes =
                    new int[]{
                            FaceLandmark.MOUTH_BOTTOM,
                            FaceLandmark.MOUTH_RIGHT,
                            FaceLandmark.MOUTH_LEFT,
                            FaceLandmark.RIGHT_EYE,
                            FaceLandmark.LEFT_EYE,
                            FaceLandmark.RIGHT_EAR,
                            FaceLandmark.LEFT_EAR,
                            FaceLandmark.RIGHT_CHEEK,
                            FaceLandmark.LEFT_CHEEK,
                            FaceLandmark.NOSE_BASE
                    };
            String[] landMarkTypesStrings =
                    new String[]{
                            "MOUTH_BOTTOM",
                            "MOUTH_RIGHT",
                            "MOUTH_LEFT",
                            "RIGHT_EYE",
                            "LEFT_EYE",
                            "RIGHT_EAR",
                            "LEFT_EAR",
                            "RIGHT_CHEEK",
                            "LEFT_CHEEK",
                            "NOSE_BASE"
                    };
            for (int i = 0; i < landMarkTypes.length; i++) {
                FaceLandmark landmark = face.getLandmark(landMarkTypes[i]);
                if (landmark == null) {

                } else {
                    PointF landmarkPosition = landmark.getPosition();
                    String landmarkPositionStr =
                            String.format(Locale.US, "x: %f , y: %f", landmarkPosition.x, landmarkPosition.y);
                }
            }

        }
    }


    private void processFaces(List<Face> faces,Image mediaImage) {

        Log.d(TAG, "processFaces onSuccess: called");
        Log.d(TAG, "processFaces: faces size :: " + faces.size());
        if (faces.size() ==1) {


            /*for (Face face : faces) {
//                textView.setText(faces.size()+" face detected");
                checkFaceMask(face);
                *//*if (checkFaceMask(face)){
                    checkEyeBlink(face);
                    checkFaceRotation(face);
                }*//*
            }*/

//            checkEyeBlink(faces.get(0));
//            getFaceLandmarks(faces.get(0));

            checkLiveNessOfFace(faces.get(0));

         /*   Bitmap rotatedBitmap;
            Face face = faces.get(0);
            Bitmap frame_bmp = toBitmap(mediaImage);
            Matrix rotationMatrix = new Matrix();
            if(frame_bmp.getWidth() >= frame_bmp.getHeight()){
                rotationMatrix.setRotate(90);
            }else{
                rotationMatrix.setRotate(0);
            }

            rotatedBitmap = Bitmap.createBitmap(frame_bmp,0,0,frame_bmp.getWidth(),frame_bmp.getHeight(),rotationMatrix,true);
            liveBitmap = Bitmap.createBitmap(frame_bmp,0,0,frame_bmp.getWidth(),frame_bmp.getHeight(),rotationMatrix,true);
            BitmapHolder.getInstance().setLiveImageBitmap(liveBitmap);

            RectF boundingBox = new RectF(face.getBoundingBox());

            //Crop out bounding box from whole Bitmap(image)
            Bitmap cropped_face = getCropBitmapByCPU(rotatedBitmap, boundingBox);
            Bitmap scaled = getResizedBitmap(cropped_face, 112, 112);

            detectFaceMask(scaled);*/


//            Log.d(TAG, "processFaces: "+scaled);
        } else {
            textView.setText(" No face detected");
        }

    }

    private void checkLiveNessOfFace(Face face) {
        try{
            if (face!=null){
                List<FaceContour> faceContours = face.getAllContours();
                int totalContourSize = 0;
                for (FaceContour contour:faceContours){
                    totalContourSize+=contour.getPoints().size();
                }

                Log.d(TAG, "checkLiveNessOfFace: totalContourSize :: "+totalContourSize);

//            Log.d(TAG, "checkLiveNessOfFace: getSmilingProbability :: "+face.getSmilingProbability());
                if (FaceDetectionActivity.isCaptureBtnClicked){
                    getImageFromInputImage();
//                    captureImageUsingImageCapture();
                }else {
                    float smilingProb = face.getSmilingProbability();

                    if ( smilingProb<0.1){
                        boolean isMovementDetected = false;
//                Log.d(TAG, "checkLiveNessOfFace: mouth is visible check face rotation "+smilingProb);
                        if (face.getLeftEyeOpenProbability()<0.5){
                            isMovementDetected = true;
                        }

                        if (face.getRightEyeOpenProbability()<0.5){
                            isMovementDetected = true;
                        }

                        if (checkFaceRotation(face)){
                            isMovementDetected = true;
                        }

                        if (isMovementDetected){
                            Log.d(TAG, "checkLiveNessOfFace: movement detected person is live");
                            if (!isCaptureEnabled){
                                enableImageCapture();
                            }
                        }else{
                            Log.d(TAG, "checkLiveNessOfFace: no movement detected ");
                        }
                    }
                }

            }

        }catch (Exception exception){
            Log.d(TAG, "checkLiveNessOfFace: exception "+exception.getLocalizedMessage());
        }
    }



    private void enableImageCapture() {
        if (countDownTimer==null){
            countDownTimer = new CountDownTimer(3000,2000) {
                @Override
                public void onTick(long l) {
                    isCaptureEnabled = true;
                    movementListener.isMovementDetected(true);
                }

                @Override
                public void onFinish() {
                    isCaptureEnabled = false;
                    movementListener.isMovementDetected(false);
                    countDownTimer = null;
                }
            };
            countDownTimer.start();
        }


    }

    private void getFaceLandmarks(Face face) {
        List<FaceLandmark> faceLandmarkList = face.getAllLandmarks();
        FaceLandmark leftEar = face.getLandmark(FaceLandmark.LEFT_EAR);
        if (leftEar != null) {
            PointF leftEarPos = leftEar.getPosition();
        }else {
            Log.d(TAG, "getFaceLandmarks: leftEar is null");
        }
        FaceLandmark leftEye = face.getLandmark(FaceLandmark.LEFT_EYE);
        if (leftEye != null) {
            PointF leftEyePos = leftEye.getPosition();
        }else {
            Log.d(TAG, "getFaceLandmarks: leftEye is null");

        }
        FaceLandmark leftMouth = face.getLandmark(FaceLandmark.MOUTH_LEFT);
        if (leftMouth != null) {
            PointF leftMouthPos = leftMouth.getPosition();
        }else {
            Log.d(TAG, "getFaceLandmarks: leftMouth is null");

        }
    }


    private Bitmap toBitmap(Image image) {

        byte[] nv21=YUV_420_888toNV21(image);


        YuvImage yuvImage = new YuvImage(nv21, ImageFormat.NV21, image.getWidth(), image.getHeight(), null);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, yuvImage.getWidth(), yuvImage.getHeight()), 75, out);

        byte[] imageBytes = out.toByteArray();
        //System.out.println("bytes"+ Arrays.toString(imageBytes));

        //System.out.println("FORMAT"+image.getFormat());

        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

    //IMPORTANT. If conversion not done ,the toBitmap conversion does not work on some devices.
    private static byte[] YUV_420_888toNV21(Image image) {

        int width = image.getWidth();
        int height = image.getHeight();
        int ySize = width*height;
        int uvSize = width*height/4;

        byte[] nv21 = new byte[ySize + uvSize*2];

        ByteBuffer yBuffer = image.getPlanes()[0].getBuffer(); // Y
        ByteBuffer uBuffer = image.getPlanes()[1].getBuffer(); // U
        ByteBuffer vBuffer = image.getPlanes()[2].getBuffer(); // V

        int rowStride = image.getPlanes()[0].getRowStride();
        assert(image.getPlanes()[0].getPixelStride() == 1);

        int pos = 0;

        if (rowStride == width) { // likely
            yBuffer.get(nv21, 0, ySize);
            pos += ySize;
        }
        else {
            long yBufferPos = -rowStride; // not an actual position
            for (; pos<ySize; pos+=width) {
                yBufferPos += rowStride;
                yBuffer.position((int) yBufferPos);
                yBuffer.get(nv21, pos, width);
            }
        }

        rowStride = image.getPlanes()[2].getRowStride();
        int pixelStride = image.getPlanes()[2].getPixelStride();

        assert(rowStride == image.getPlanes()[1].getRowStride());
        assert(pixelStride == image.getPlanes()[1].getPixelStride());

        if (pixelStride == 2 && rowStride == width && uBuffer.get(0) == vBuffer.get(1)) {
            // maybe V an U planes overlap as per NV21, which means vBuffer[1] is alias of uBuffer[0]
            byte savePixel = vBuffer.get(1);
            try {
                vBuffer.put(1, (byte)~savePixel);
                if (uBuffer.get(0) == (byte)~savePixel) {
                    vBuffer.put(1, savePixel);
                    vBuffer.position(0);
                    uBuffer.position(0);
                    vBuffer.get(nv21, ySize, 1);
                    uBuffer.get(nv21, ySize + 1, uBuffer.remaining());

                    return nv21; // shortcut
                }
            }
            catch (ReadOnlyBufferException ex) {
                // unfortunately, we cannot check if vBuffer and uBuffer overlap
            }

            // unfortunately, the check failed. We must save U and V pixel by pixel
            vBuffer.put(1, savePixel);
        }

        // other optimizations could check if (pixelStride == 1) or (pixelStride == 2),
        // but performance gain would be less significant

        for (int row=0; row<height/2; row++) {
            for (int col=0; col<width/2; col++) {
                int vuPos = col*pixelStride + row*rowStride;
                nv21[pos++] = vBuffer.get(vuPos);
                nv21[pos++] = uBuffer.get(vuPos);
            }
        }

        return nv21;
    }

    private static Bitmap getCropBitmapByCPU(Bitmap source, RectF cropRectF) {
        Bitmap resultBitmap = Bitmap.createBitmap((int) cropRectF.width(),
                (int) cropRectF.height(), Bitmap.Config.ARGB_8888);
        Canvas cavas = new Canvas(resultBitmap);

        // draw background
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setColor(Color.WHITE);
        cavas.drawRect(
                new RectF(0, 0, cropRectF.width(), cropRectF.height()),
                paint);

        Matrix matrix = new Matrix();
        matrix.postTranslate(-cropRectF.left, -cropRectF.top);

        cavas.drawBitmap(source, matrix, paint);
//        BitmapHolder.getInstance().setLiveFaceImage(source);

        if (source != null && !source.isRecycled()) {
            source.recycle();
        }

        return resultBitmap;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }


    private boolean checkFaceMask(Face face) {
        if (face!=null){
            FaceLandmark leftMouth = face.getLandmark(FaceLandmark.MOUTH_LEFT);
            FaceLandmark rightMouth = face.getLandmark(FaceLandmark.MOUTH_RIGHT);
            FaceLandmark bottomMouth = face.getLandmark(FaceLandmark.MOUTH_BOTTOM);
            FaceLandmark nose = face.getLandmark(FaceLandmark.NOSE_BASE);

            if (nose!=null){
                Log.d(TAG, "checkFaceMask: nose "+nose.getPosition().x+" "+nose.getPosition().y);
                Log.d(TAG, "checkFaceMask: nose "+nose.getPosition().length());
                Log.d(TAG, "checkFaceMask: nose "+nose.getPosition().describeContents());
            }else{
                Log.d(TAG, "checkFaceMask: nose is null");
            }

            if (leftMouth!=null){
                PointF lmp = leftMouth.getPosition();

                Log.d(TAG, "checkFaceMask: leftmouth "+lmp);
            }else{
                Log.d(TAG, "checkFaceMask: leftmouth is null");
            }

            if (rightMouth!=null){
                PointF lmp = rightMouth.getPosition();

                Log.d(TAG, "checkFaceMask: rightmouth "+lmp);
            }else{
                Log.d(TAG, "checkFaceMask: rightmouth is null");
            }

            if (bottomMouth!=null){
                PointF lmp = bottomMouth.getPosition();

                Log.d(TAG, "checkFaceMask: bottommouth "+lmp);
            }else{
                Log.d(TAG, "checkFaceMask: bottommouth is null");
            }
            Log.d(TAG, "checkFaceMask: smiling prob "+face.getSmilingProbability());

            List<FaceContour> upperLipBottomContour =
                    face.getAllContours();
           /* List<PointF> lowerLipBottomContour =
                    face.getContour(FaceContour.LOWER_LIP_BOTTOM).getPoints();
            List<PointF> upperLipTopContour =
                    face.getContour(FaceContour.UPPER_LIP_TOP).getPoints();
            List<PointF> lowerLipTopContour =
                    face.getContour(FaceContour.LOWER_LIP_TOP).getPoints();

            Log.d(TAG, "checkFaceMask: counterSize"+upperLipTopContour.size() +" "+upperLipBottomContour.size() +" "+lowerLipBottomContour.size()+" "+lowerLipTopContour.size());
            */
            if(!upperLipBottomContour.isEmpty()){
                Log.d(TAG, "checkFaceMask: counterSize "+upperLipBottomContour.size());
            }else{
                Log.d(TAG, "checkFaceMask: counterSize is null");
            }
        }
        return true;
    }



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private int getRotationCompensation(String cameraId, Activity activity, boolean isFrontFacing, int rotDegree)
            throws CameraAccessException {
        // Get the device's current rotation relative to its "native" orientation.
        // Then, from the ORIENTATIONS table, look up the angle the image must be
        // rotated to compensate for the device's rotation.
        int deviceRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int rotationCompensation = ORIENTATIONS.get(deviceRotation);


        // Get the device's sensor orientation.
        int sensorOrientation = cameraManager
                .getCameraCharacteristics(cameraId)
                .get(CameraCharacteristics.SENSOR_ORIENTATION);

        if (isFrontFacing) {
            rotationCompensation = (sensorOrientation + rotDegree) % 360;
        } else { // back-facing
            rotationCompensation = (sensorOrientation - rotDegree + 360) % 360;
        }
        return rotationCompensation;
    }

/*
    private String detectMask(Bitmap bitmap){
        // load model
        String modelFile = FileUtil.loadMappedFile(context, "model.tflite");
        val model = Interpreter(modelFile, Interpreter.Options());
        val labels = FileUtil.loadLabels(this, "labels.txt")

        // data type
        val imageDataType = model.getInputTensor(0).dataType()
        val inputShape = model.getInputTensor(0).shape()

        val outputDataType = model.getOutputTensor(0).dataType()
        val outputShape = model.getOutputTensor(0).shape()

        var inputImageBuffer = TensorImage(imageDataType)
        val outputBuffer = TensorBuffer.createFixedSize(outputShape, outputDataType)

        // preprocess
        val cropSize = kotlin.math.min(input.width, input.height)
        val imageProcessor = ImageProcessor.Builder()
                .add(ResizeWithCropOrPadOp(cropSize, cropSize))
                .add(ResizeOp(inputShape[1], inputShape[2], ResizeOp.ResizeMethod.NEAREST_NEIGHBOR))
                .add(NormalizeOp(127.5f, 127.5f))
                .build()

        // load image
        inputImageBuffer.load(input)
        inputImageBuffer = imageProcessor.process(inputImageBuffer)

        // run model
        model.run(inputImageBuffer.buffer, outputBuffer.buffer.rewind())

        // get output
        val labelOutput = TensorLabel(labels, outputBuffer)

        val label = labelOutput.mapWithFloatValue
        return label
    }
*/

    private void captureImageUsingImageCapture(){
       ImageCapture imageCapture = new ImageCapture.Builder().setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY).build();

        long timestamp = System.currentTimeMillis();

        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, timestamp);
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");

        ImageCapture.Metadata metadata = new ImageCapture.Metadata();
        metadata.setReversedHorizontal(true); // This method will fix mirror issue


        ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(
                context.getContentResolver(),
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues).setMetadata(metadata).build();
        imageCapture.takePicture(
                outputFileOptions, executor, new ImageCapture.OnImageSavedCallback () {
                    @Override
                    public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {

                                Log.d("filePath", "run: "+outputFileResults.getSavedUri());
                                Uri imageUri = outputFileResults.getSavedUri();
                                Log.d(TAG, "run: Image saved Successfully");
                                if (imageUri!=null){
                                    Intent intent = new Intent(context,FullImageView.class).putExtra("liveness_image_uri",imageUri);
                                    context.startActivity(intent);
                                }else{
                                    DialogFactory.dismissCameraProgressDialog();
                                }
                            }
                        });
                    }
                    @Override
                    public void onError(@NonNull ImageCaptureException error) {
                        error.printStackTrace();
                    }
                });
    }





}
