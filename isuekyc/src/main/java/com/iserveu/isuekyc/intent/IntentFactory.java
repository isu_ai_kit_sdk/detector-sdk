package com.iserveu.isuekyc.intent;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.iserveu.isuekyc.ActivityHome;
import com.iserveu.isuekyc.aadhaar_back.AadhaarBackCameraActivity;
import com.iserveu.isuekyc.aadhaar_back.AadhaarBackDataActivity;
import com.iserveu.isuekyc.aadhaar_back.AadhaarBackImageReviewActivity;
import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontCameraActivity;
import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontDataActivity;
import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontImageReviewActivity;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.face_detection.FaceDetectionActivity;
import com.iserveu.isuekyc.hyperverge.AadhaarFrontHVCameraActivity;
import com.iserveu.isuekyc.hyperverge.AadhaarLivnessHVCameraActivity;
import com.iserveu.isuekyc.pan_card.PanCameraActivity;
import com.iserveu.isuekyc.pan_card.PanImageReviewActivity;
import com.iserveu.isuekyc.utils.EKycConsentAgreementActivity;

public class IntentFactory {
    public static String responsePackageName = "";
    public static Activity responseActivityName;

    private static IntentFactory intentFactoryISUeKyc;

    public static IntentFactory getInstance() {
        if (intentFactoryISUeKyc == null) {
            intentFactoryISUeKyc = new IntentFactory();
        }
        return intentFactoryISUeKyc;
    }

    public static Intent returnAadhaarFrontHVCameraActivity(Context context) {
        return new Intent(context, AadhaarFrontHVCameraActivity.class);
    }

    public static Intent returnAadhaarFaceHVCameraActivity(Context context) {
        return new Intent(context, AadhaarLivnessHVCameraActivity.class);
    }

    public static Intent returnAadhaarFrontCameraActivity(Context context) {
        return new Intent(context, AadhaarFrontCameraActivity.class);
    }

    public static Intent returnEKycConsentAgreementActivity(Context context) {
        return new Intent(context, EKycConsentAgreementActivity.class);
    }

    public static Intent returnAadhaarBackCameraActivity(Context context) {
        return new Intent(context, AadhaarBackCameraActivity.class);
    }

    public static Intent returnPanCameraActivity(Context context) {
        return new Intent(context, PanCameraActivity.class);
    }

    public static Intent returnFaceDetectionActivity(Context context) {
        return new Intent(context, FaceDetectionActivity.class);
    }

    public static Intent returnLivenessFaceDetectionActivity(Context context) {
        return new Intent(context, com.iserveu.isuekyc.liveness.FaceDetectionActivity.class);
    }

    public static Intent returnFaceMatchActivity(Context context) {
        return new Intent(context, FaceDetectionActivity.class);
    }

    public static Intent returnAadhaarFrontImageReviewActivity(Context context) {
        return new Intent(context, AadhaarFrontImageReviewActivity.class);
    }

    public static Intent returnAadhaarBackImageReviewActivity(Context context) {
        return new Intent(context, AadhaarBackImageReviewActivity.class);
    }

    public static Intent returnPanImageReviewActivity(Context context) {
        return new Intent(context, PanImageReviewActivity.class);
    }

    public static Intent returnAadhaarFrontDataActivity(Context context) {
        return new Intent(context, AadhaarFrontDataActivity.class);
    }

    public static Intent returnAadhaarBackDataActivity(Context context) {
        return new Intent(context, AadhaarBackDataActivity.class);
    }

    public static Intent returnActivityHomeActivity(Context context) {
        return new Intent(context, ActivityHome.class);
    }

    public static Intent returnAppMainActivity(Context context) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(context, "com.isuisudmt.posonboarding.ekyc.AadhaarVerificationActivity"));
//        intent.setComponent(new ComponentName(context, "com.example.isuekyc.MainActivity"));
        return intent;
//        return new Intent(context,com.example.isuekyc.MainActivity.class);
    }
    public static Intent returnAppPanActivity(Context context) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(context, "com.isuisudmt.posonboarding.ekyc.PanVerificationActivity"));
//        intent.setComponent(new ComponentName(context, "com.example.isuekyc.MainActivity"));
        return intent;
//        return new Intent(context,com.example.isuekyc.MainActivity.class);
    }

    public static void launchIsuEKycActivity(Context context, String type) {
        if (type.equalsIgnoreCase("aadharfront")) {
            Intent intent = new Intent(context, AadhaarFrontCameraActivity.class);
            context.startActivity(intent);
        } else if (type.equalsIgnoreCase("aadharback")) {
            Intent intent = IntentFactory.returnAadhaarBackCameraActivity(context);
            context.startActivity(intent);
        } else if (type.equalsIgnoreCase("pancard")) {
            Intent intent = IntentFactory.returnPanCameraActivity(context);
            context.startActivity(intent);
        } else if (type.equalsIgnoreCase("faceliveness")) {
            Intent intent = new Intent(context, com.iserveu.isuekyc.liveness.FaceDetectionActivity.class);
            context.startActivity(intent);
//                Utils.getActivityFromContext(context).finish();
        } else if (type.equalsIgnoreCase("consent")) {
            Intent intent = IntentFactory.returnEKycConsentAgreementActivity(context);
            context.startActivity(intent);
        } else if (type.equalsIgnoreCase("aadharfronthv")) {
            Intent intent = new Intent(context, AadhaarFrontHVCameraActivity.class);
            context.startActivity(intent);
        } else if (type.equalsIgnoreCase("facelivenesshv")) {
            Intent intent = new Intent(context, AadhaarLivnessHVCameraActivity.class);
            context.startActivity(intent);
        } else if (type.equalsIgnoreCase("facematch")) {
            if (BitmapHolder.getInstance().getAadhaarFaceImageEncoded() != null) {
                Intent intent = IntentFactory.returnFaceMatchActivity(context);
                context.startActivity(intent);
            } else {
                Toast.makeText(context, "Capture aadhar image first ", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(context, "Invalid key", Toast.LENGTH_SHORT).show();
        }


    }
}
