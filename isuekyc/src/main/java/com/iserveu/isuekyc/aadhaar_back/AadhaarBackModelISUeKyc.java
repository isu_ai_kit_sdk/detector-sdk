package com.iserveu.isuekyc.aadhaar_back;

public class AadhaarBackModelISUeKyc {
    private static AadhaarBackModelISUeKyc aadhaarBackModelISUeKyc;
    private String aadhaarNumber;
    private String aadhaarVIDNumber;
    private String aadhaarState;
    private String aadhaarPIN;
    private String aadhaarAddress;
    private String backImageUrl;

    public String getBackImageUrl() {
        return backImageUrl;
    }

    public void setBackImageUrl(String backImageUrl) {
        this.backImageUrl = backImageUrl;
    }

    public static AadhaarBackModelISUeKyc getInstance(){
        if (aadhaarBackModelISUeKyc==null){
            aadhaarBackModelISUeKyc = new AadhaarBackModelISUeKyc();
        }
        return aadhaarBackModelISUeKyc;
    }

    public String getAadhaarNumber() {
        return aadhaarNumber;
    }

    public AadhaarBackModelISUeKyc setAadhaarNumber(String aadhaarNumber) {
        this.aadhaarNumber = aadhaarNumber;
        return aadhaarBackModelISUeKyc;
    }

    public String getAadhaarVIDNumber() {
        return aadhaarVIDNumber;
    }

    public AadhaarBackModelISUeKyc setAadhaarVIDNumber(String aadhaarVIDNumber) {
        this.aadhaarVIDNumber = aadhaarVIDNumber;
        return aadhaarBackModelISUeKyc;
    }

    public String getAadhaarState() {
        return aadhaarState;
    }

    public AadhaarBackModelISUeKyc setAadhaarState(String aadhaarState) {
        this.aadhaarState = aadhaarState;
        return aadhaarBackModelISUeKyc;
    }

    public String getAadhaarPIN() {
        return aadhaarPIN;
    }

    public AadhaarBackModelISUeKyc setAadhaarPIN(String aadhaarPIN) {
        this.aadhaarPIN = aadhaarPIN;
        return aadhaarBackModelISUeKyc;

    }

    public String getAadhaarAddress() {
        return aadhaarAddress;
    }

    public AadhaarBackModelISUeKyc setAadhaarAddress(String aadhaarAddress) {
        this.aadhaarAddress = aadhaarAddress;
        return aadhaarBackModelISUeKyc;
    }
}
