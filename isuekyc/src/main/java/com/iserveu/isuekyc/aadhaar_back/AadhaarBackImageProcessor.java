package com.iserveu.isuekyc.aadhaar_back;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;
import com.iserveu.isuekyc.Constants;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.dialogs.DialogFactory;
import com.iserveu.isuekyc.intent.IntentFactory;

import java.util.List;

public class AadhaarBackImageProcessor {

    private static final String TAG = AadhaarBackImageProcessor.class.getSimpleName();
    private static AadhaarBackImageProcessor aadhaarBackImageProcessor;
    private Context context;
    private boolean isBackPageFound = false,isFrontPageFound = false;
    private String state="",pinCode="";
    private String address="",stateAndPinCode="",aadharNo="",vid="";
    private ProgressDialog progressDialog;



    public static AadhaarBackImageProcessor getInstance(){
        if (aadhaarBackImageProcessor==null){
            aadhaarBackImageProcessor = new AadhaarBackImageProcessor();
        }
        return aadhaarBackImageProcessor;
    }

    public void processAadhaarBackImage(Context context, Bitmap aadhaarBackImage){
//        DialogFactory.showImageProcessingDialog(context,"Please wait...");
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        InputImage image = InputImage.fromBitmap(aadhaarBackImage,0);
        TextRecognizer recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
        recognizer.process(image).addOnSuccessListener(new OnSuccessListener<Text>() {
            @Override
            public void onSuccess(Text text) {
                validateAadhaarBackImage(context,text);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnCompleteListener(new OnCompleteListener<Text>() {
            @Override
            public void onComplete(@NonNull Task<Text> task) {

            }
        });
    }

    private void validateAadhaarBackImage(Context context, Text text) {

        Log.d(TAG, "validateAadhaarBackImage: complete_text "+text.getText());
        List<Text.TextBlock> blocks =  text.getTextBlocks();
        if (blocks.size() == 0) {
            Toast.makeText(context, "No text found", Toast.LENGTH_SHORT).show();
            DialogFactory.dismissDialog();
            dismissProgressDialog();
            return;
        }

        for (int i=0;i<blocks.size();i++){

            String cw = blocks.get(i).getText();
            String cwLower = blocks.get(i).getText().toLowerCase();

            /*getPossibleLanguageOfAString(cw,languageIdentifier);
            identifyLanguageOfAString(cw,languageIdentifier);*/
            if (cwLower.contains("ress") || cwLower.contains("dress") || cwLower.contains("address")){
                isBackPageFound = true;
            }

            if (cw.contains("male") || cw.contains("female") || cw.contains("dob") ){
                isFrontPageFound = true;
            }

            if (cw.contains("ress") || cw.contains("dress") || cw.contains("Address")){
                isBackPageFound = true;
            }
            if (cw.contains("MALE") || cw.contains("FEMALE") || cw.contains("DOB") ){
                isFrontPageFound = true;
            }
        }

       if (isBackPageFound){
            extractAadhaarBackPageData(context,text);
        }else{
            Toast.makeText(context, "Please capture back page of aadhaar card", Toast.LENGTH_SHORT).show();
            dismissProgressDialog();
            DialogFactory.dismissDialog();
            return;
        }

    }

    private void extractAadhaarBackPageData(Context context, Text text) {
        this.context=context;

        List<Text.TextBlock> blocks = text.getTextBlocks();
        for (int i = 0; i < blocks.size(); i++) {
//            Log.d("blocks", "Blocks: " + i + blocks.get(i).getText());
            if (blocks.get(i).getText().contains("ddres")){
                List<Text.Line> addressLines = blocks.get(i).getLines();
                int addressBlk = i+1;
                address = blocks.get(i).getText();
                if (address.length() <=10){
                    try {
                        address = blocks.get(addressBlk).getText();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }


            if (blocks.get(i).getText().contains("-")){
                String str = blocks.get(i).getText();
                if (countDigits(str)==6){
                    stateAndPinCode = str;
                    getStateAndPinCode(stateAndPinCode);
                }

            }
            List<Text.Line> lines = blocks.get(i).getLines();
            for (int j = 0; j < lines.size(); j++) {
                Log.d("blocks", "Lines:" + i + " " + lines.get(j).getText() + lines.get(j).getText().length());


                if (lines.get(j).getText().contains("-")){
                    String str = lines.get(j).getText();
                    if (countDigits(str)==6){
                        stateAndPinCode = str;
                        getStateAndPinCode(stateAndPinCode);
                    }

                }
                if (lines.get(j).getText().length() == 14){
                    aadharNo = lines.get(j).getText();
                }
                if (lines.get(j).getText().contains("VID")){
                    vid = lines.get(j).getText();
                }
                if (lines.get(j).getText().length() == 14 && lines.get(j).getText().trim().length()==12){
                    aadharNo = lines.get(j).getText();
                }
                if (lines.get(j).getText().contains("VID")){
                    vid = lines.get(j).getText();
                }

            }
        }
        getValidatedVID();
        validateAadhaarNo();
        validateUserAddress();
        DialogFactory.dismissDialog();
        dismissProgressDialog();
        showData(state,pinCode,address,aadharNo,vid);
    }

    private void validateUserAddress() {
        Log.d(TAG, "validateUserAddress: address-> "+address);
        if (address!=null){
            // checking address: added or not
            if (address.contains(":") ){
                address = address.replace(":","");
            }
            if (address.contains("Address") ){
                address = address.replace("Address","");
            }

            if (address.contains("AddresS")){
                address = address.replace("AddresS","");
            }

            if (address.contains("Address")){
                address = address.replace("Address","");
            }
            if (address.contains("\n")){
                address = address.replaceAll("\n","");
            }

            // checking state and pin code added or not
            if (address.charAt(address.length()-1)==',' || address.charAt(address.length()-1)=='.' ){
                if (countDigits(address)<6){
                    address+=stateAndPinCode;
                }
            }
        }

        Log.d(TAG, "validateUserAddress: newAddress-> "+address);

    }

    private void getValidatedVID() {
        String[] splitVid ;
        if (vid != null){
            splitVid = vid.split(":");
            if (splitVid.length==2){
                vid = splitVid[1];
            }
        }
        Log.d(TAG, "getValidatedVID: "+vid);
    }

    private boolean validateAadhaarNo() {
        Log.d(TAG, "validateAadhaarNo: "+aadharNo);
        if (aadharNo ==null){
//            snackBarText +="Aadhaar No, ";
            Log.d(TAG, "getValidatedGender: can't find aadharNo");
        }
        return countDigits(aadharNo) == 12;

    }


    private void showData(String state, String pinCode, String address, String aadharNo, String vid) {
        dismissProgressDialog();
        AadhaarBackModelISUeKyc.getInstance()
                .setAadhaarNumber(aadharNo)
                .setAadhaarVIDNumber(vid)
                .setAadhaarState(state).setAadhaarPIN(pinCode)
                .setAadhaarAddress(address)
                .setBackImageUrl(Constants.backImageUri);

        context.startActivity(IntentFactory.returnAppMainActivity(context)
                .putExtra("imgType", "aadharBack"));

    }

    private void getStateAndPinCode(String stateAndPinCode) {
        Log.d(TAG, "getStateAndPinCode: stateAndPinCode "+stateAndPinCode);
        String[] splitSnP ;
        if (stateAndPinCode!=null){
            splitSnP = stateAndPinCode.split("-");
            if(splitSnP.length==2){
                state = splitSnP[0];
                pinCode = splitSnP[1];
            }
        }

    }

    private int countDigits(String str){
        int digits = 0;
        if (str!=null){
            for (int m=0;m<str.length();m++){
                Character c = str.charAt(m);
                if (Character.isDigit(c)){
                    digits++;
                }
            }
        }
        Log.d(TAG, "countDigits: word "+str +" digits "+digits);
        return digits;
    }

    private void dismissProgressDialog(){
        if (progressDialog!=null){
            progressDialog.dismiss();
        }
    }
}
