package com.iserveu.isuekyc.aadhaar_front;

import android.net.Uri;

public class AadhaarFrontModelISUeKyc {
    private static AadhaarFrontModelISUeKyc aadhaarFrontModelISUeKyc;
    private String aadhaarName;
    private String aadhaarFatherName;
    private String aadhaarDOB;
    private String aadhaarGender;
    private String aadhaarNumber;
    private String aadhaarVIDNumber;
    private String aadhaarEncodedUserImage;
    private String frontImageUrl;


    public String getFrontImageUrl() {
        return frontImageUrl;
    }

    public void setFrontImageUrl(String frontImageUrl) {
        this.frontImageUrl = frontImageUrl;
    }

    public static AadhaarFrontModelISUeKyc getInstance(){
        if (aadhaarFrontModelISUeKyc==null){
            aadhaarFrontModelISUeKyc = new AadhaarFrontModelISUeKyc();
        }
        return aadhaarFrontModelISUeKyc;
    }

    public String getAadhaarName() {
        return aadhaarName;
    }

    public AadhaarFrontModelISUeKyc setAadhaarName(String aadhaarName) {
        this.aadhaarName = aadhaarName;
        return aadhaarFrontModelISUeKyc;
    }

    public String getAadhaarFatherName() {
        return aadhaarFatherName;
    }

    public AadhaarFrontModelISUeKyc setAadhaarFatherName(String aadhaarFatherName) {
        this.aadhaarFatherName = aadhaarFatherName;
        return aadhaarFrontModelISUeKyc;
    }

    public String getAadhaarDOB() {
        return aadhaarDOB;
    }

    public AadhaarFrontModelISUeKyc setAadhaarDOB(String aadhaarDOB) {
        this.aadhaarDOB = aadhaarDOB;
        return aadhaarFrontModelISUeKyc;

    }

    public String getAadhaarGender() {
        return aadhaarGender;
    }

    public AadhaarFrontModelISUeKyc setAadhaarGender(String aadhaarGender) {
        this.aadhaarGender = aadhaarGender;
        return aadhaarFrontModelISUeKyc;
    }

    public String getAadhaarNumber() {
        return aadhaarNumber;
    }

    public AadhaarFrontModelISUeKyc setAadhaarNumber(String aadhaarNumber) {
        this.aadhaarNumber = aadhaarNumber;
        return aadhaarFrontModelISUeKyc;
    }

    public String getAadhaarVIDNumber() {
        return aadhaarVIDNumber;
    }

    public AadhaarFrontModelISUeKyc setAadhaarVIDNumber(String aadhaarVIDNumber) {
        this.aadhaarVIDNumber = aadhaarVIDNumber;
        return aadhaarFrontModelISUeKyc;
    }

    public String getAadhaarEncodedUserImage() {
        return aadhaarEncodedUserImage;
    }

    public AadhaarFrontModelISUeKyc setAadhaarEncodedUserImage(String aadhaarEncodedUserImage) {
        this.aadhaarEncodedUserImage = aadhaarEncodedUserImage;
        return aadhaarFrontModelISUeKyc;
    }
}
