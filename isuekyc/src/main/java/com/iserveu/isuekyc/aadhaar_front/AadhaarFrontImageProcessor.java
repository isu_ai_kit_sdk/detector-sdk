package com.iserveu.isuekyc.aadhaar_front;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;
import com.iserveu.isuekyc.Constants;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.dialogs.DialogFactory;
import com.iserveu.isuekyc.face_detection.CapturedRecognition;
import com.iserveu.isuekyc.face_detection.CurrentRecognition;
import com.iserveu.isuekyc.intent.IntentFactory;

import org.tensorflow.lite.Interpreter;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AadhaarFrontImageProcessor {
    private static final String TAG = AadhaarFrontImageProcessor.class.getSimpleName();
    private Context context;private static AadhaarFrontImageProcessor aadhaarFrontImageProcessor;
    private boolean isBackPageFound = false,isFrontPageFound = false,isFatherNameFound= false,isMotherNameFound=false;
    private String dob="",name="",gender="",aadharNo="",vid="",fatherName="",motherName="", userImageEncoded ="no",aadhaarFrontImageEncoded="";
    private boolean isUserImageSaved= false,isUserSignatureSaved = false;
    private ProgressDialog progressDialog;
    private Bitmap bitmap;
    private float[][] embeedings;
    private HashMap<String, CapturedRecognition.Recognition> registered = new HashMap<>(); //saved Faces
    int OUTPUT_SIZE=192;
    private int inputSize = 112;
    private int[] intValues;
    private boolean isModelQuantized=false;
    private float IMAGE_MEAN = 128.0f;
    private float IMAGE_STD = 128.0f;
    private Interpreter tfLite;
    private boolean developerMode=false;
    private float distance= 1.0f;
    private String modelFile="mobile_face_net.tflite"; //model name




    public static AadhaarFrontImageProcessor getInstance(){
        if (aadhaarFrontImageProcessor==null){
            aadhaarFrontImageProcessor = new AadhaarFrontImageProcessor();
        }
        return aadhaarFrontImageProcessor;
    }

    public void processAadhaarFrontImage(Context context, Bitmap aadhaarFrontImage){
        this.context = context;
        this.bitmap = aadhaarFrontImage;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
//        DialogFactory.showImageProcessingDialog(context,"Please wait...");
        InputImage image = InputImage.fromBitmap(aadhaarFrontImage,0);
        TextRecognizer recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
        recognizer.process(image).addOnSuccessListener(new OnSuccessListener<Text>() {
            @Override
            public void onSuccess(Text text) {
                progressDialog.setMessage("Wait...");
                validateAadhaarFrontImage(context,text);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.setTitle("Slow internet connection !");
                progressDialog.setMessage(e.getLocalizedMessage());
//                dismissProgressDialog();
                Log.d(TAG, "onFailure: "+e.getLocalizedMessage());

            }
        }).addOnCompleteListener(new OnCompleteListener<Text>() {
            @Override
            public void onComplete(@NonNull Task<Text> task) {
            }
        });
    }



    private void validateAadhaarFrontImage(Context context, Text text) {
        Log.d(TAG, "validateAadhaarFrontImage: complete_text "+text.getText());
        List<Text.TextBlock> blocks =  text.getTextBlocks();
        if (blocks.size() == 0) {
            Toast.makeText(context, "No text found", Toast.LENGTH_SHORT).show();
//            DialogFactory.dismissDialog();
            return;
        }

        for (int i=0;i<blocks.size();i++){

            String cw = blocks.get(i).getText();
            /*getPossibleLanguageOfAString(cw,languageIdentifier);
            identifyLanguageOfAString(cw,languageIdentifier);*/
            if (cw.contains("ress") || cw.contains("dress") || cw.contains("Address")){
                isBackPageFound = true;
            }
            if (cw.contains("MALE") || cw.contains("FEMALE") || cw.contains("DOB") ){
                isFrontPageFound = true;
            }

            if (cw.contains("Fath") || cw.contains("Father")  ){
                isFatherNameFound = true;
            }
            if (cw.contains("Moth") || cw.contains("Mother")  ){
                isMotherNameFound = true;
            }
        }

        if (isFrontPageFound){
            if (isFatherNameFound){
                extractAadhaarFrontPageDataWithFatherName(context,text);
            }else if (isMotherNameFound){
                extractAadhaarFrontPageDataWithMotherName(context,text);
            }else{
                extractAadhaarFrontPageData(context,text);
            }
        }else{
            Toast.makeText(context, "Please capture front page of aadhaar card", Toast.LENGTH_SHORT).show();
            dismissProgressDialog();
            DialogFactory.dismissDialog();
            return;
        }

    }

    public void detectFace(Context mContext,Bitmap mBitmap){

        InputImage image = InputImage.fromBitmap(mBitmap,0);
        FaceDetectorOptions options =
                new FaceDetectorOptions.Builder()
                        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                        .setMinFaceSize(0.15f)
                        .enableTracking()
                        .build();

        FaceDetector detector = FaceDetection.getClient(options);
        detector.process(image).addOnSuccessListener(new OnSuccessListener<List<Face>>() {
            @Override
            public void onSuccess(List<Face> faces) {
                if (faces!=null){
                    if (faces.size()>0){
                        processFaces(faces,mBitmap);
                    }else{
                        dismissProgressDialog();
                        Toast.makeText(context, "no Face detected", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnCompleteListener(new OnCompleteListener<List<Face>>() {
            @Override
            public void onComplete(@NonNull Task<List<Face>> task) {

            }
        });

    }


    private void processFaces(List<Face> faces,Bitmap  bitmap) {
        if (faces.size()==1){
            Log.d(TAG, "processFaces: One face detected");
            Face currFace =faces.get(0);
            encodeDetectedFace(currFace,bitmap);
            saveFaceDetailsToTrainModel(currFace,bitmap);

            dismissProgressDialog();
        }else  if (faces.size()>1){
//            Toast.makeText(context, "Multiple faces detected", Toast.LENGTH_SHORT).show();
            dismissProgressDialog();
            showData(dob,name,gender,aadharNo,vid,isUserImageSaved);
            userImageEncoded = "multiple_faces";
        }else{
            Toast.makeText(context, "No faces detected", Toast.LENGTH_SHORT).show();
            dismissProgressDialog();
            showData(dob,name,gender,aadharNo,vid,isUserImageSaved);
            userImageEncoded = "no_faces";
        }
    }

    private void saveFaceDetailsToTrainModel(Face currFace, Bitmap bitmap) {
//        registered = readFromSP();
        Rect bounds = currFace.getBoundingBox();
        saveDetectedFace(currFace,bitmap);
        RectF boundingBox = new RectF(currFace.getBoundingBox());
        Bitmap cropped_face = getCropBitmapByCPU(bitmap, boundingBox);
        Bitmap scaled = getResizedBitmap(cropped_face, 112, 112);
//        Bitmap actualFace = Bitmap.createBitmap(bitmap,bounds.left, bounds.top, bounds.width(), bounds.height());


        recognizeImage(scaled);



    }
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
//        bm.recycle();
        return resizedBitmap;
    }

    private static Bitmap getCropBitmapByCPU(Bitmap source, RectF cropRectF) {
        Bitmap resultBitmap = Bitmap.createBitmap((int) cropRectF.width(),
                (int) cropRectF.height(), Bitmap.Config.ARGB_8888);
        Canvas cavas = new Canvas(resultBitmap);

        // draw background
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setColor(Color.WHITE);
        cavas.drawRect(
                new RectF(0, 0, cropRectF.width(), cropRectF.height()),
                paint);

        Matrix matrix = new Matrix();
        matrix.postTranslate(-cropRectF.left, -cropRectF.top);

        cavas.drawBitmap(source, matrix, paint);

        if (source != null && !source.isRecycled()) {
            source.recycle();
        }

        return resultBitmap;
    }

    public void recognizeImage(final Bitmap bitmap) {
        if (bitmap!=null){
            Log.d(TAG, "recognizeImage: called");
            //Create ByteBuffer to store normalized image

            ByteBuffer imgData = ByteBuffer.allocateDirect(1 * inputSize * inputSize * 3 * 4);

            imgData.order(ByteOrder.nativeOrder());

            intValues = new int[inputSize * inputSize];

            //get pixel values from Bitmap to normalize
            bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

            imgData.rewind();

            for (int i = 0; i < inputSize; ++i) {
                for (int j = 0; j < inputSize; ++j) {
                    int pixelValue = intValues[i * inputSize + j];
                    if (isModelQuantized) {
                        // Quantized model
                        imgData.put((byte) ((pixelValue >> 16) & 0xFF));
                        imgData.put((byte) ((pixelValue >> 8) & 0xFF));
                        imgData.put((byte) (pixelValue & 0xFF));
                    } else { // Float model
                        imgData.putFloat((((pixelValue >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                        imgData.putFloat((((pixelValue >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                        imgData.putFloat(((pixelValue & 0xFF) - IMAGE_MEAN) / IMAGE_STD);

                    }
                }
            }
            //imgData is input to our model
            Object[] inputArray = {imgData};

            Map<Integer, Object> outputMap = new HashMap<>();


            embeedings = new float[1][OUTPUT_SIZE]; //output of model will be stored in this variable
            Log.d(TAG, "recognizeImage: embeedings :: "+embeedings.toString());

            outputMap.put(0, embeedings);

            try{




                tfLite=new Interpreter(loadModelFile((Activity) context,modelFile));
                tfLite.runForMultipleInputsOutputs(inputArray, outputMap); //Run model

                /*CapturedRecognition.Recognition  recognition = new CapturedRecognition.Recognition("0", "", -1f);
                recognition.setExtra(embeedings);*/
                CapturedRecognition.Recognition.getInstance().initClassifier("0", "", -1f).setExtra(embeedings);
                registered.put(name,CapturedRecognition.Recognition.getInstance());

                Object objCurrent = CurrentRecognition.Recognition.getInstance().getExtra();
                Object objCaptured = CapturedRecognition.Recognition.getInstance().getExtra();


                /*SimilarityClassifierHandler.Recognition.getInstance().initClassifier( "0", "", -1f).setCapturedExtra(embeedings);
                Object obj = SimilarityClassifierHandler.Recognition.getInstance().getCapturedExtra();
                Log.d(TAG, "recognizeImage: obj "+obj);
                registered.put(name,SimilarityClassifierHandler.Recognition.getInstance());
                Log.d(TAG, "recognizeImage: embeedings :: "+ Arrays.deepToString(embeedings));
                Log.d(TAG, "saveFaceDetailsToTrainModel: SimilarityClassifierHandler "+SimilarityClassifierHandler.Recognition.getInstance());
//                insertToSP(registered,0);
                Log.d(TAG, "saveFaceDetailsToTrainModel: registered "+registered.toString());*/

            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "recognizeImage: exception "+e.getLocalizedMessage());
            }
        }else{
            Log.d(TAG, "recognizeImage: bitmap is null");
        }

        showData(dob,name,gender,aadharNo,vid,isUserImageSaved);
        
    }

    private MappedByteBuffer loadModelFile(Activity activity, String MODEL_FILE) throws IOException {
        AssetFileDescriptor fileDescriptor = activity.getAssets().openFd(MODEL_FILE);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }



    private void encodeAadhaarFrontImage( Bitmap bitmap) {


        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            aadhaarFrontImageEncoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            BitmapHolder.getInstance().setAadhaarFrontImageEncoded(aadhaarFrontImageEncoded);
            Log.d(TAG, "encodeAadhaarFrontImage: "+aadhaarFrontImageEncoded);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Please put full image inside camera view ", Toast.LENGTH_SHORT).show();
        }
    }

    private void encodeDetectedFace(Face currFace, Bitmap bitmap) {
        Rect bounds = currFace.getBoundingBox();

        try {
            Bitmap croppedBitmapPrev = Bitmap.createBitmap(bitmap,bounds.left-10, bounds.top-70, bounds.width()+20, bounds.height()+50);
            Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left-10, bounds.top-70, bounds.width()+20, bounds.height()+50);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            userImageEncoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            BitmapHolder.getInstance().setAadhaarFaceImageEncoded(userImageEncoded);
            Log.d(TAG, "encodeDetectedFace: "+userImageEncoded);
            encodeAadhaarFrontImage(bitmap);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Please put full image inside camera view ", Toast.LENGTH_SHORT).show();
        }
    }


    private void saveDetectedFace(Face currFace,Bitmap bitmap) {
        Rect bounds = currFace.getBoundingBox();
        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left-50, bounds.top-150, bounds.width()+100, bounds.height()+200);
        try {

            //Write file
            String filename = "detected_user_image_aadhaar.png";
            FileOutputStream stream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            //Cleanup
            stream.close();
            croppedBitmap.recycle();
            isUserImageSaved = true;
            Log.d(TAG, "saveDetectedFace: detected face saved");



//            //Pop intent
//            Intent in1 = new Intent(context, FullImageView.class);
//            in1.putExtra(Constants.DETECTED_FACE_FROM_DOCUMENT, filename);
//            context.startActivity(in1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void extractAadhaarFrontPageData(Context context, Text text) {
        int dobLinePosition =0;
        int dobNameBlock =0,nameBlock=0;


        List<Text.TextBlock> blocks = text.getTextBlocks();
        for (int i = 0; i < blocks.size(); i++) {
            Log.d("blocks", "Blocks: " + i + blocks.get(i).getText());
            String currWord = blocks.get(i).getText();


            if (blocks.get(i).getText().contains("ndi") || blocks.get(i).getText().contains("India") || blocks.get(i).getText().contains("NDI") || blocks.get(i).getText().contains("INDIA")){
                nameBlock = i;
                try {
                    name = blocks.get(nameBlock + 1).getText();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (blocks.get(i).getText().contains("DOB")){

                dobNameBlock = i;
                try {
                    name = blocks.get(dobNameBlock-1).getText();

                    if (blocks.get(i+1).getText().contains("MALE")){
                        gender = blocks.get(i+1).getText();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            List<Text.Line> lines = blocks.get(i).getLines();
            for (int j = 0; j < lines.size(); j++) {
                Log.d("blocks", "Lines:" + i + " " + lines.get(j).getText() + lines.get(j).getText().length());
                if (lines.get(j).getText().contains("DOB")) {
                    dob = lines.get(j).getText();
                    dobLinePosition = j;
                    try {
                        name = lines.get(dobLinePosition - 1).getText();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (lines.get(j).getText().contains("MALE")){
                    gender = lines.get(j).getText();
                    if (gender.contains("FEMALE")){
                        gender = "FEMALE";
                    }else if (gender.contains("MALE")){
                        gender = "MALE";
                    }
                    Log.d(TAG, "extractAadhaarFrontPageData: gender "+gender);
                }


                if (lines.get(j).getText().length() == 14){
                    aadharNo = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: aadharNo "+aadharNo);
                }
                if (lines.get(j).getText().contains("VID")){
                    vid = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: vid "+vid);
                }
                if (lines.get(j).getText().length() == 14 && lines.get(j).getText().trim().length()==12){
                    aadharNo = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: aadharNo 2 "+aadharNo);
                }
                if (lines.get(j).getText().contains("VID")){
                    vid = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: vid 2"+vid);
                }

            }
        }

        getValidatedVID();
        validateAadhaarNo();
        validateName();
        getValidatedGender();
        getValidatedDOB();
//        showData(dob,name,gender,aadharNo,vid,false);







        /*CountDownTimer intentTimer = new CountDownTimer(3000,1000) {
            @Override
            public void onTick(long l) {

            }
            @Override
            public void onFinish() {
                showData(dob,name,gender,aadharNo,vid);
            }
        };
        intentTimer.start();*/

        DialogFactory.dismissDialog();
    }

    private void extractAadhaarFrontPageDataWithFatherName(Context context, Text text) {
        int dobLinePosition =0;
        int dobNameBlock =0,nameBlock=0;


        List<Text.TextBlock> blocks = text.getTextBlocks();
        for (int i = 0; i < blocks.size(); i++) {
            Log.d("blocks", "Blocks: " + i + blocks.get(i).getText());
            String currWord = blocks.get(i).getText();

            if (currWord.contains("Fath") || currWord.contains("Father")){
                getFatherName(currWord);
            }

            if (currWord.contains("Moth")||currWord.contains("Mother")){
                getMotherName(currWord);
            }

            if (blocks.get(i).getText().contains("ndi") || blocks.get(i).getText().contains("India") || blocks.get(i).getText().contains("NDI") || blocks.get(i).getText().contains("INDIA")){
                nameBlock = i;
                try {
                    name = blocks.get(nameBlock + 1).getText();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (blocks.get(i).getText().contains("DOB")){

                dobNameBlock = i;
                try {
                    String prevOfDOBBlock = blocks.get(dobNameBlock-1).getText();
                    if (prevOfDOBBlock.contains("Fath") || prevOfDOBBlock.contains("Father")){
                        getFatherName(currWord);
                    }

                    if (prevOfDOBBlock.contains("Moth") || prevOfDOBBlock.contains("Mother")){
                        getMotherName(currWord);
                    }

                    if (blocks.get(i+1).getText().contains("MALE")){
                        gender = blocks.get(i+1).getText();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            List<Text.Line> lines = blocks.get(i).getLines();
            for (int j = 0; j < lines.size(); j++) {
                Log.d("blocks", "Lines:" + i + " " + lines.get(j).getText() + lines.get(j).getText().length());
                if (lines.get(j).getText().contains("DOB")) {
                    dob = lines.get(j).getText();
                    dobLinePosition = j;
                    try {
                       String currText = lines.get(dobLinePosition - 1).getText();
                       if (currText.contains("Fath") || currText.contains("Father")){
                           getFatherName(currText);
                       }
                        if (currText.contains("Moth") || currText.contains("Mother")){
                            getMotherName(currText);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (lines.get(j).getText().contains("MALE")){
                    gender = lines.get(j).getText();
                    if (gender.contains("FEMALE")){
                        gender = "FEMALE";
                    }else if (gender.contains("MALE")){
                        gender = "MALE";
                    }
                    Log.d(TAG, "extractAadhaarFrontPageData: gender "+gender);

                    dob = lines.get(j-1).getText();
                }


                if (lines.get(j).getText().length() == 14){
                    aadharNo = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: aadharNo "+aadharNo);
                }
                if (lines.get(j).getText().contains("VID")){
                    vid = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: vid "+vid);
                }
                if (lines.get(j).getText().length() == 14 && lines.get(j).getText().trim().length()==12){
                    aadharNo = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: aadharNo 2 "+aadharNo);
                }
                if (lines.get(j).getText().contains("VID")){
                    vid = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: vid 2"+vid);
                }

            }
        }

        getValidatedVID();
        validateAadhaarNo();
        validateName();
        getValidatedGender();
        getValidatedDOB();
//        showData(dob,name,gender,aadharNo,vid,isUserImageSaved);
        DialogFactory.dismissDialog();
    }

    private void extractAadhaarFrontPageDataWithMotherName(Context context, Text text) {
        int dobLinePosition =0;
        int dobNameBlock =0,nameBlock=0;


        List<Text.TextBlock> blocks = text.getTextBlocks();
        for (int i = 0; i < blocks.size(); i++) {
            Log.d("blocks", "Blocks: " + i + blocks.get(i).getText());
            String currWord = blocks.get(i).getText();

            if (currWord.contains("Fath") || currWord.contains("Father")){
                getFatherName(currWord);
            }

            if (currWord.contains("Moth")||currWord.contains("Mother")){
                getMotherName(currWord);
            }

            if (blocks.get(i).getText().contains("ndi") || blocks.get(i).getText().contains("India")){
                nameBlock = i;
                try {
                    name = blocks.get(nameBlock + 1).getText();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (blocks.get(i).getText().contains("DOB")){

                dobNameBlock = i;
                try {
                    String prevOfDOBBlock = blocks.get(dobNameBlock-1).getText();
                    if (prevOfDOBBlock.contains("Fath") || prevOfDOBBlock.contains("Father")){
                        getFatherName(currWord);
                    }

                    if (prevOfDOBBlock.contains("Moth") || prevOfDOBBlock.contains("Mother")){
                        getMotherName(currWord);
                    }

                    if (blocks.get(i+1).getText().contains("MALE")){
                        gender = blocks.get(i+1).getText();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            List<Text.Line> lines = blocks.get(i).getLines();
            for (int j = 0; j < lines.size(); j++) {
                Log.d("blocks", "Lines:" + i + " " + lines.get(j).getText() + lines.get(j).getText().length());
                if (lines.get(j).getText().contains("DOB")) {
                    dob = lines.get(j).getText();
                    dobLinePosition = j;
                    try {
//                        name = lines.get(dobLinePosition - 1).getText();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (lines.get(j).getText().contains("MALE")){
                    gender = lines.get(j).getText();
                    if (gender.contains("FEMALE")){
                        gender = "FEMALE";
                    }else if (gender.contains("MALE")){
                        gender = "MALE";
                    }
                    Log.d(TAG, "extractAadhaarFrontPageData: gender "+gender);
                }


                if (lines.get(j).getText().length() == 14){
                    aadharNo = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: aadharNo "+aadharNo);
                }
                if (lines.get(j).getText().contains("VID")){
                    vid = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: vid "+vid);
                }
                if (lines.get(j).getText().length() == 14 && lines.get(j).getText().trim().length()==12){
                    aadharNo = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: aadharNo 2 "+aadharNo);
                }
                if (lines.get(j).getText().contains("VID")){
                    vid = lines.get(j).getText();
                    Log.d(TAG, "extractAadhaarFrontPageData: vid 2"+vid);
                }

            }
        }

        getValidatedVID();
        validateAadhaarNo();
        validateName();
        getValidatedGender();
        getValidatedDOB();
        showData(dob,name,gender,aadharNo,vid,isUserImageSaved);
        DialogFactory.dismissDialog();
    }


    private void getFatherName(String currWord) {
        String[] splitFatherName ;
        if (currWord!=null){
            splitFatherName = currWord.split(":");
            if (splitFatherName.length ==2){
                fatherName = splitFatherName[1];
            }
        }else{
//            snackBarText +="DOB, ";
            Log.d(TAG, "getValidatedGender: can't find DOB");
        }
        Log.d(TAG, "getValidatedDOB: "+fatherName);
    }

    private void getMotherName(String currWord) {
        String[] splitMotherName ;
        if (currWord!=null){
            splitMotherName = currWord.split(":");
            if (splitMotherName.length ==2){
                motherName = splitMotherName[1];
            }
        }else{
//            snackBarText +="DOB, ";
            Log.d(TAG, "getValidatedGender: can't find DOB");
        }
        Log.d(TAG, "getValidatedDOB: "+motherName);
    }



    private boolean validateName() {
        Log.d(TAG, "validateName: "+name);
        if (name ==null){
//            snackBarText +="Name, ";
            Log.d(TAG, "getValidatedGender: can't find name");
        }
        return name !=null;
    }

    private boolean validateAadhaarNo() {
        Log.d(TAG, "validateAadhaarNo: "+aadharNo);
        if (aadharNo ==null){
//            snackBarText +="Aadhaar No, ";
            Log.d(TAG, "getValidatedGender: can't find aadharNo");
        }
        return countDigits(aadharNo) == 12;

    }

    private void getValidatedDOB() {

        String[] splitDob ;
        if (dob!=null){
            splitDob = dob.split(":");
            if (splitDob.length ==2){
                Log.d(TAG, "getValidatedDOB: "+splitDob.toString());
                dob = splitDob[1];
            }
        }else{
//            snackBarText +="DOB, ";
            Log.d(TAG, "getValidatedGender: can't find DOB");
        }
        Log.d(TAG, "getValidatedDOB: "+dob);
        /*Thread thread = new Thread(){
            @Override
            public void run() {
                detectFace(context,bitmap);
            }
        };
        thread.start();*/

        detectFace(context,bitmap);
    }

    private void getValidatedGender() {
        String [] splitGender ;
        if (gender!=null){
            splitGender = gender.split("/");
            if (splitGender.length==2){
                gender = splitGender[1];
            }
        }else{
            Log.d(TAG, "getValidatedGender: can't find gender");
        }
        Log.d(TAG, "getValidatedGender: "+gender);
    }

    private void getValidatedVID() {
        String[] splitVid ;
        if (vid != null){
            splitVid = vid.split(":");
            if (splitVid.length==2){
                vid = splitVid[1];
            }

        }
        Log.d(TAG, "getValidatedVID: "+vid);
    }

    private int countDigits(String str){
        int digits = 0;
        if (str!=null){
            for (int m=0;m<str.length();m++){
                Character c = str.charAt(m);
                if (Character.isDigit(c)){
                    digits++;
                }
            }
        }
        Log.d(TAG, "countDigits: word "+str +" digits "+digits);
        return digits;
    }


    private void showData(String dob, String name, String gender, String aadharNo, String vid,boolean isUserImageSaved) {
        AadhaarFrontModelISUeKyc.getInstance().setAadhaarName(name)
                .setAadhaarNumber(aadharNo)
                .setAadhaarDOB(dob).setAadhaarVIDNumber(vid)
                .setAadhaarGender(gender).setAadhaarEncodedUserImage(userImageEncoded)
                .setFrontImageUrl(Constants.frontImageUri);


        context.startActivity(IntentFactory.returnAppMainActivity(context)
                        .putExtra("imgType","aadharFront"));


    }


    private void dismissProgressDialog(){
        if (progressDialog!=null){
            progressDialog.dismiss();
        }
    }

}
