package com.iserveu.isuekyc.aadhaar_front;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import com.iserveu.isuekyc.ActivityHome;
import com.iserveu.isuekyc.Constants;
import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.aadhaar_back.AadhaarBackImageProcessor;
import com.iserveu.isuekyc.databinding.ActivityAadhaarFrontImageReviewBinding;
import com.iserveu.isuekyc.intent.IntentFactory;


import java.io.IOException;

public class AadhaarFrontImageReviewActivity extends AppCompatActivity {

    private ActivityAadhaarFrontImageReviewBinding binding;
    private Bitmap mSelectedImage,rotatedBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAadhaarFrontImageReviewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

//        getSupportActionBar().hide();
        initViews();
        showCapturedImage();
    }
    private void showCapturedImage() {
        Uri imageUri = Uri.parse(getIntent().getExtras().getString("imageUri"));
        Constants.frontImageUri = imageUri.toString();
        try {
            mSelectedImage = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
//            mSelectedImage = AadhaarFrontCameraActivity.capturedBitmap;

            Matrix rotationMatrix = new Matrix();
            if(mSelectedImage.getWidth() >= mSelectedImage.getHeight()){
                rotationMatrix.setRotate(90);
            }else{
                rotationMatrix.setRotate(0);
            }

            rotatedBitmap = Bitmap.createBitmap(mSelectedImage,0,0,mSelectedImage.getWidth(),mSelectedImage.getHeight(),rotationMatrix,true);
            binding.reviewImage.setImageBitmap(rotatedBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initViews() {

        binding.confirmButton.setOnClickListener(view -> {
            AadhaarFrontImageProcessor.getInstance().processAadhaarFrontImage(this,rotatedBitmap);
        });

        binding.retakeButton.setOnClickListener(view -> {
            startActivity(IntentFactory.returnAppMainActivity(AadhaarFrontImageReviewActivity.this));
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ActivityHome.class);
        startActivity(intent);
//        startActivity(IntentFactory.returnActivityHomeActivity(AadhaarFrontCameraActivity.this));
    }
    @Override
    protected void onResume() {
        super.onResume();

    }
}