package com.iserveu.isuekyc.hyperverge;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class NewOnBoardingViewModel extends ViewModel {
    private static final String TAG = NewOnBoardingViewModel.class.getSimpleName();
    private NewOnBoardingRepository newOnBoardingRepository;

    public NewOnBoardingRepository getOnBoardingRepository() {
        if(newOnBoardingRepository ==null){
            newOnBoardingRepository = new NewOnBoardingRepository();
        }
        return newOnBoardingRepository;
    }

    /**
     * request for generate aadhaar document
     */
    public LiveData<HyperVergeDocumentModel> getAadhaarDocumentImageUrl(Context ctx){
        return getOnBoardingRepository().getAadhaarDocument(ctx);
    }

    /**
     * request for generate aadhaar document
     */
    public LiveData<HyperVergeDocumentModel> getFaceLiveSelfi(Context ctx){
        return getOnBoardingRepository().getFaceLiveSelfi(ctx);
    }

    /**
     * start ocr request for captured document
     */
    public LiveData<HyperVergeDocumentModel> fetchOcrDataFromCaptureDocuments(Context ctx,String docImageUri){
        return getOnBoardingRepository().fetchOcrDataFromCaptureDocuments(ctx,docImageUri);
    }

    /**
     * start verification of documents
     */
    public LiveData<HyperVergeDocumentModel> verifyDocument(Context ctx,String docImage,String selfiDocUri){
        return getOnBoardingRepository().verifyDocument(ctx,docImage,selfiDocUri);
    }
}
