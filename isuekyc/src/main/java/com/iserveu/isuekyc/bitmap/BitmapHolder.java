package com.iserveu.isuekyc.bitmap;

import android.graphics.Bitmap;
import android.util.Log;

public class BitmapHolder {

    private static final String TAG = BitmapHolder.class.getSimpleName();
    private static BitmapHolder bitmapHolder;


    private String aadhaarFrontImageEncoded;
    private String capturedAadhaarBackImageEncoded;
    private String capturedPanImageEncoded;
    private String panSignatureImageEncoded;
    private String aadhaarFaceImageEncoded;
    private String panFaceImageEncoded;
    private String liveFaceImageEncoded;
    private Bitmap liveImageBitmap;
    private Bitmap liveNessCheckBitmap;
    private String setLiveNessImageEncoded;

    public Bitmap getLiveNessCheckBitmap() {
        return liveNessCheckBitmap;
    }

    public void setLiveNessCheckBitmap(Bitmap liveNessCheckBitmap) {
        this.liveNessCheckBitmap = liveNessCheckBitmap;
    }

    public static BitmapHolder getInstance(){
        if (bitmapHolder==null){
            bitmapHolder = new BitmapHolder();
        }
        return bitmapHolder;
    }

    public  void clearData(){
        if (bitmapHolder!=null){
            bitmapHolder = null;
        }
    }

    public String getCapturedAadhaarBackImageEncoded() {
        return capturedAadhaarBackImageEncoded;
    }

    public void setCapturedAadhaarBackImageEncoded(String capturedAadhaarBackImageEncoded) {
        this.capturedAadhaarBackImageEncoded = capturedAadhaarBackImageEncoded;
    }

    public Bitmap getLiveImageBitmap() {
        return liveImageBitmap;
    }

    public void setLiveImageBitmap(Bitmap liveImageBitmap) {
        this.liveImageBitmap = liveImageBitmap;
    }

    public String getCapturedPanImageEncoded() {
        return capturedPanImageEncoded;
    }

    public void setCapturedPanImageEncoded(String capturedPanImageEncoded) {
        this.capturedPanImageEncoded = capturedPanImageEncoded;
    }

    public String getPanSignatureImageEncoded() {
        return panSignatureImageEncoded;
    }

    public void setPanSignatureImageEncoded(String panSignatureImageEncoded) {
        this.panSignatureImageEncoded = panSignatureImageEncoded;
    }

    public String getAadhaarFaceImageEncoded() {
        return aadhaarFaceImageEncoded;
    }

    public void setAadhaarFaceImageEncoded(String aadhaarFaceImageEncoded) {
        this.aadhaarFaceImageEncoded = aadhaarFaceImageEncoded;
    }

    public String getSetLiveNessImageEncoded() {
        return setLiveNessImageEncoded;
    }

    public void setSetLiveNessImageEncoded(String setLiveNessImageEncoded) {
        this.setLiveNessImageEncoded = setLiveNessImageEncoded;
    }

    public String getPanFaceImageEncoded() {
        return panFaceImageEncoded;
    }

    public void setPanFaceImageEncoded(String panFaceImageEncoded) {
        this.panFaceImageEncoded = panFaceImageEncoded;
    }

    public String getLiveFaceImageEncoded() {
        return liveFaceImageEncoded;
    }

    public void setLiveFaceImageEncoded(String liveFaceImageEncoded) {
        Log.d(TAG, "setLiveFaceImageEncoded: "+liveFaceImageEncoded);
        this.liveFaceImageEncoded = liveFaceImageEncoded;
    }




    public String getAadhaarFrontImageEncoded() {
        return aadhaarFrontImageEncoded;
    }

    public void setAadhaarFrontImageEncoded(String aadhaarFrontImageEncoded) {
        Log.d(TAG, "setAadhaarFrontImageEncoded: "+aadhaarFrontImageEncoded);
        this.aadhaarFrontImageEncoded = aadhaarFrontImageEncoded;
    }


}
