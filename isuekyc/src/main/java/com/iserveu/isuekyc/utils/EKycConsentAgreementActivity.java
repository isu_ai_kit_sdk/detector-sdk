package com.iserveu.isuekyc.utils;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.databinding.ActivityEkycConsentAgreementBinding;

public class EKycConsentAgreementActivity extends AppCompatActivity {
    private ActivityEkycConsentAgreementBinding binding;
    private boolean isAgeChecked = false;
    private boolean isResidenceChecked = false;
    private boolean isPhysicallyPresentChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEkycConsentAgreementBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        binding.ekycAgreementAge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isAgeChecked = b;
                enableProceedButton();
            }
        });

        binding.ekycAgreementPresent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isPhysicallyPresentChecked= b;
                enableProceedButton();
            }
        });

        binding.ekycAgreementResidence.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isResidenceChecked = b;
                enableProceedButton();
            }
        });

    }

    private void enableProceedButton(){
        if (isAgeChecked && isResidenceChecked && isPhysicallyPresentChecked){
            binding.ekycAgreementProceed.setVisibility(View.VISIBLE);
        }else{
            binding.ekycAgreementProceed.setVisibility(View.GONE);
        }
    }
}