package com.iserveu.isuekyc.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class Utils {


    public static boolean isAllPermissionsGranted(Context context,String[] REQUIRED_PERMISSIONS ){
        boolean isAllGranted = true;
        for (String permission:REQUIRED_PERMISSIONS){
            if (ContextCompat.checkSelfPermission(context,permission)!= PackageManager.PERMISSION_GRANTED){
                isAllGranted = false;
            }
        }
        return isAllGranted;
    }
}
