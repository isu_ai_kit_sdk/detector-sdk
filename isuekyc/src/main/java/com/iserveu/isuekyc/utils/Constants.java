package com.iserveu.isuekyc.utils;

public class Constants {
    public static final String IMAGE_TYPE = "image_code";
    public static final String AADHAAR_FRONT_PAGE_ = "aadhaar_front_page";
    public static final String AADHAAR_BACK_PAGE = "aadhaar_back_page";
    public static final String PAN_CARD = "pan_card";
    public static final String FACE_FROM_DOCUMENTS = "face_from_document";
    public static final String DETECTED_FACE_FROM_DOCUMENT = "detected_face_bitmap_from_document";
    public static final String DETECTED_SIGNATURE_FROM_DOCUMENT = "detected_signature_bitmap_from_document";
    public static final String SIGNATURE_DETECTION_FROM_DOCUMENT = "signature_detection_from_document";

}
