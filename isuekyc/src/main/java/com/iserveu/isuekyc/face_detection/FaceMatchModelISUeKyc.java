package com.iserveu.isuekyc.face_detection;

public class FaceMatchModelISUeKyc {
    private static FaceMatchModelISUeKyc faceMatchModelISUeKyc;
    private String faceImageEncoded;

    public static FaceMatchModelISUeKyc getInstance(){
        if (faceMatchModelISUeKyc==null){
            faceMatchModelISUeKyc = new FaceMatchModelISUeKyc();
        }
        return faceMatchModelISUeKyc;
    }

    public String getFaceImageEncoded() {
        return faceImageEncoded;
    }

    public FaceMatchModelISUeKyc setFaceImageEncoded(String faceImageEncoded) {
        this.faceImageEncoded = faceImageEncoded;
        return faceMatchModelISUeKyc;
    }
}
