package com.iserveu.isuekyc.face_detection;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.databinding.ActivitySelfieConsentBinding;

public class SelfieConsentActivity extends AppCompatActivity {
    private ActivitySelfieConsentBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySelfieConsentBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


    }
}