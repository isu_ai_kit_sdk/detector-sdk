package com.iserveu.isuekyc.face_detection;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.util.concurrent.ListenableFuture;
import com.iserveu.isuekyc.ActivityHome;
import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.intent.IntentFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FaceDetectionActivity extends AppCompatActivity {

    private static final String TAG = FaceDetectionActivity.class.getSimpleName();
    private Preview preview;
    private PreviewView previewView,preview_border;
    private Camera camera;
    private RelativeLayout view;
    private FloatingActionButton button;
    private ImageView cameraControllerBtn;
    private ImageView capturedImageView;
    private ImageView imgDone;
    private ImageView imgCancel;
    private Bitmap bitmap;
    private TextView faceStatusTv,taskTextView,timerTextView;
    public static boolean isTimerActive = false;
    private FaceMatchListener faceMatchListener;
    public static  boolean shouldEncodeImage = false;
    private ProgressDialog progressDialog;
    private Button proceed_selfie_button;
    private ConstraintLayout selfie_camera_layout;
    private RelativeLayout selfie_consent_layout;
    private TextView captureText;
    private ImageView close_selfie_camera;
    ExecutorService executor;
    public static boolean isCaptureBtnClicked = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_detection);
        //Load model


        initViews();
        renderImage();




    }



    private void initViews() {
        if (ContextCompat.checkSelfPermission(FaceDetectionActivity.this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(FaceDetectionActivity.this, new String[] {Manifest.permission.CAMERA}, 101);
        }

        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }

        captureText = findViewById(R.id.capture_text);
        previewView = findViewById(R.id.pv);
        preview_border = findViewById(R.id.pv_border_face_detection);
        view = findViewById(R.id.conlay);
        faceStatusTv = findViewById(R.id.textView3);
        taskTextView = findViewById(R.id.task_Tv);
        taskTextView.setTag("");
        timerTextView = findViewById(R.id.timer_textView);
        cameraControllerBtn = findViewById(R.id.camera_controller_btn);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Wait...");
        proceed_selfie_button = findViewById(R.id.proceed_selfie_capture_btn);
        selfie_camera_layout = findViewById(R.id.selfie_camera_layout);
        selfie_consent_layout = findViewById(R.id.selfie_consent_layout);

        imgCancel = findViewById(R.id.imgCancel);
        capturedImageView = findViewById(R.id.imgCapture);
        imgDone = findViewById(R.id.imgDone);
        close_selfie_camera = findViewById(R.id.close_selfie_camera);

        selfie_consent_layout.setVisibility(View.VISIBLE);
        selfie_camera_layout.setVisibility(View.GONE);


        close_selfie_camera.setOnClickListener(view1 -> {
            finishAffinity();
        });
        proceed_selfie_button.setOnClickListener(view1 -> {
            selfie_consent_layout.setVisibility(View.GONE);
            selfie_camera_layout.setVisibility(View.VISIBLE);
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideImagePreview();
            }
        });

        cameraControllerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                renderImage();
                taskTextView.setTag("active");
                isTimerActive = true;
                cameraControllerBtn.setClickable(false);
                cameraControllerBtn.setBackgroundResource(R.drawable.ic_camera_on);
                CountDownTimer countDownTimer = new CountDownTimer(4000,1000) {
                    @Override
                    public void onTick(long l) {
                        isTimerActive = true;
                        long sec = l/1000;
                        timerTextView.setText(sec+" Seconds remaining");

                    }

                    @Override
                    public void onFinish() {
                        taskTextView.setTag("inactive");
                        isTimerActive = false;
                        cameraControllerBtn.setClickable(true);
                        cameraControllerBtn.setBackgroundResource(R.drawable.ic_camera_off);
                        timerTextView.setText(" Time ended");
                        imgDone.setVisibility(View.VISIBLE);
                        imgCancel.setVisibility(View.VISIBLE);
                        cameraControllerBtn.setVisibility(View.GONE);

                    }
                };
                countDownTimer.start();

            }
        });

        capturedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bitmap != null){
                    Intent intent = new Intent(FaceDetectionActivity.this, FullImageView.class);
                    startActivity(intent);
                }
            }
        });
        imgDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    isCaptureBtnClicked = true;
                    shouldEncodeImage = true;
                    progressDialog.show();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        Intent intent = new Intent(IntentFactory.returnAppMainActivity(FaceDetectionActivity.this)).putExtra("imgType","faceMatch_with_aadhaar");
                        startActivity(intent);

                    }
                },2000);

//                    Toast.makeText(FaceDetectionActivity.this, "done", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void hideImagePreview() {
        capturedImageView.setVisibility(View.GONE);
        imgCancel.setVisibility(View.GONE);
        imgDone.setVisibility(View.GONE);
        cameraControllerBtn.setVisibility(View.VISIBLE);
    }


    private void renderImage() {
        Log.d(TAG, "renderImage: called");
        Log.d(TAG, "renderImage: isTimerActive "+isTimerActive);
        executor = Executors.newSingleThreadExecutor();

        ListenableFuture<ProcessCameraProvider> val = ProcessCameraProvider.getInstance(this);

        val.addListener(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.P)
            @Override
            public void run() {
                Log.d(TAG, "renderImage run: called");
                try {
                    ProcessCameraProvider processCameraProvider= val.get();
                    preview = new Preview.Builder().setTargetResolution(new Size(900,1200)).build();

                    CameraSelector cameraSelector = new CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_FRONT).build();

                    preview.setSurfaceProvider(previewView.getSurfaceProvider());

                    ImageCapture imageCapture = new ImageCapture.Builder().setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY).build();
                    ImageAnalysis imageAnalysis1 = new ImageAnalysis.Builder()
                            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                            .setImageQueueDepth(10)
                            .setTargetResolution(new Size(900,1200)).build();

                    imageAnalysis1.setAnalyzer(executor
                            ,new ImageAnalyzer(FaceDetectionActivity.this, previewView, faceStatusTv, taskTextView,timerTextView,imgDone,captureText, capturedImageView, false, new FaceMatchListener() {
                                @Override
                                public void faceMatchingStatus(boolean isMatching) {
                                    if (isMatching){
                                        preview_border.setBackground(getResources().getDrawable(R.drawable.green_preview_view_border));
                                    }else{
                                        preview_border.setBackground(getResources().getDrawable(R.drawable.red_preview_view_border));

                                    }
                                }
                            }));

                    processCameraProvider.unbindAll();
                    camera = processCameraProvider.bindToLifecycle(FaceDetectionActivity.this,cameraSelector,preview,imageAnalysis1);
                } catch (ExecutionException e) {
                    Log.d(TAG, "run: "+e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    Log.d(TAG, "run: "+e.getLocalizedMessage());
                    e.printStackTrace();
                }

            }
        }, ContextCompat.getMainExecutor(FaceDetectionActivity.this));

    }


    @Override
    protected void onResume() {
        super.onResume();
        shouldEncodeImage = false;
    }
}