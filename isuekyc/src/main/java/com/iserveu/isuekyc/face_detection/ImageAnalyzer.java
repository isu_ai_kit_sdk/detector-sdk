package com.iserveu.isuekyc.face_detection;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;

import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.YuvImage;
import android.media.Image;

import android.os.CountDownTimer;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageInfo;
import androidx.camera.core.ImageProxy;
import androidx.camera.view.PreviewView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.common.internal.ImageConvertUtils;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.google.mlkit.vision.face.FaceLandmark;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.intent.IntentFactory;

import org.tensorflow.lite.Interpreter;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.ReadOnlyBufferException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImageAnalyzer implements ImageAnalysis.Analyzer,FaceMatchListener {

    private static final String TAG = ImageAnalyzer.class.getSimpleName();
    Context context;
    private PreviewView rootview;
    private TextView textView,taskTextView,timerTextView;
    private ImageView capturedImgView;
    private Bitmap liveBitmap;
    private int eyeBlinkCount =0;
    private Bitmap bitmap;
    private boolean isTimerActive;
    private FaceMatchListener faceMatchListener;
    private InputImage image;
    private int inputSize = 112;
    private int[] intValues;
    private boolean isModelQuantized=false;
    private float IMAGE_MEAN = 128.0f;
    private float IMAGE_STD = 128.0f;
    private int OUTPUT_SIZE=192;
    private float[][] embeedings;
    private Interpreter tfLite;
    private boolean developerMode=false;
    private float distance= 1.0f;
    private String modelFile="mobile_face_net.tflite"; //model name
    private HashMap<String, CurrentRecognition.Recognition> registered = new HashMap<>(); //saved Faces
    private float[][] capturedEmb;
    private ImageView imgDone;
    private TextView captureText;





    public ImageAnalyzer(Context context, PreviewView view, TextView textView, TextView taskTextView, TextView timerTextView, ImageView imgDone,TextView captureText, ImageView mCapturedImgView, boolean isTimerActive, FaceMatchListener mFaceMatchListener) {

        this.context = context;
        this.rootview = view;
        this.textView = textView;
        this.capturedImgView = mCapturedImgView;
        this.taskTextView = taskTextView;
        this.isTimerActive = isTimerActive;
        this.imgDone = imgDone;
        this.faceMatchListener = mFaceMatchListener;
        this.timerTextView = timerTextView;
        this.captureText = captureText;
        SharedPreferences sharedPref = context.getSharedPreferences("Distance",Context.MODE_PRIVATE);
        distance = sharedPref.getFloat("distance",1.00f);
        initTensorFlow();

    }

    private void initTensorFlow() {
        try{
            tfLite=new Interpreter(loadModelFile((Activity) context,modelFile));
        }catch (Exception exception){
            Log.d(TAG, "initTensorFlow: fail to load tflite model ");
        }
    }

    public ImageAnalyzer(Context mContext,Bitmap mBitmap){

    }

    @Override
    public void analyze(@NonNull ImageProxy imageProxy) {
        Log.d(TAG, "analyze: called");
        @SuppressLint("UnsafeOptInUsageError")
        Image mediaImage = imageProxy.getImage();
        ImageInfo imageInfo = imageProxy.getImageInfo();


        if (mediaImage != null) {
            image = InputImage.fromMediaImage(mediaImage, imageProxy.getImageInfo().getRotationDegrees());

            FaceDetectorOptions options =
                    new FaceDetectorOptions.Builder()
                            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                            .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                            .setMinFaceSize(0.15f)
                            .enableTracking()
                            .build();

            FaceDetector detector = FaceDetection.getClient(options);
            detector.process(image).addOnSuccessListener(new OnSuccessListener<List<Face>>() {
                @Override
                public void onSuccess(List<Face> faces) {

                            processFaces(faces,mediaImage);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "analyze onFailure: "+e.getLocalizedMessage());
                }
            }).addOnCompleteListener(new OnCompleteListener<List<Face>>() {
                @Override
                public void onComplete(@NonNull Task<List<Face>> task) {
                    imageProxy.close();
                }
            });

        }

    }






    private boolean checkFaceRotation(Face face) {
        boolean isDetected= false;
        if (face!=null) {
            float rotX = face.getHeadEulerAngleX();  // Head is rotated to the left rotX degrees
            float rotY = face.getHeadEulerAngleY();  // Head is rotated to the right rotY degrees
            float rotZ = face.getHeadEulerAngleZ();  // Head is tilted sideways rotZ degrees

            Log.d(TAG, "checkFaceRotation: " +rotX+" "+rotY +" "+rotZ);
            if (rotX>10){
                Log.d(TAG, "checkFaceRotation: rotated to top" );
//                Toast.makeText(context, "face towards top", Toast.LENGTH_SHORT).show();
                isDetected = true;
            }else{
                Log.d(TAG, "checkFaceRotation: face is straight");

            }

            if (rotY>10){
                Log.d(TAG, "checkFaceRotation: rotated to  left" );
//                textView.setText(" face towards left");
                isDetected = true;
            }else{
                Log.d(TAG, "checkFaceRotation: face is straight");

            }
            if (rotX<-10){
                Log.d(TAG, "checkFaceRotation: rotated to bottom" );
//                Toast.makeText(context, "face towards bottom", Toast.LENGTH_SHORT).show();
                isDetected = true;

            }else{
                Log.d(TAG, "checkFaceRotation: face is straight");

            }

            if (rotY<-10){
                Log.d(TAG, "checkFaceRotation: rotated to right");
                textView.setText(" face towards right");
                isDetected = true;
            }else{
                Log.d(TAG, "checkFaceRotation: face is straight");

            }


        }
        return isDetected;
    }

/*
    private void checkEyeBlink(Face face) {
        */
/*if (taskTextView.getTag().equals("active")){
            taskTextView.setText("Blink eye for 2 times");
        }*//*

        Log.d(TAG, "checkEyeBlink: isTimerActive "+isTimerActive);
        if (face!=null){
            if (face.getRightEyeOpenProbability() != null){
                Log.d(TAG, "checkEyeBlink: left eye is not null");
            }

            if (face.getRightEyeOpenProbability() != null && face.getLeftEyeOpenProbability() != null) {
                Log.d(TAG, "checkEyeBlink: Both eyes are no null");
                float rightEyeOpenProb = face.getRightEyeOpenProbability();
                float leftEyeOpenProb = face.getLeftEyeOpenProbability();
                if (rightEyeOpenProb<=0.1 && leftEyeOpenProb<= 0.1){

                            eyeBlinkCount++;
                            textView.setText(" Eye Blinked "+eyeBlinkCount);
                            if (eyeBlinkCount==2 ){
                                CountDownTimer countDownTimer = new CountDownTimer(2000,500) {
                                    @Override
                                    public void onTick(long l) {

                                    }
                                    @Override
                                    public void onFinish() {
                                        getImageFromInputImage();
                                        */
/*Bitmap bitmap = generateDynamicBitmap(rootview);
                                        createImageFromBitmap(bitmap);
                                        goToFullImageActivity();*//*

                                        eyeBlinkCount=0;
                                    }
                                };
                                countDownTimer.start();

                        }

                }else{
                    Log.d(TAG, "checkEyeBlink: Eye is open");
//                    eyeBlinkDetectedListener.onEyeBlinked(false);
                }
            }else{
                Log.d(TAG, "checkEyeBlink: Eye is null");
            }
        }else{
            Log.d(TAG, "checkEyeBlink: face is null");
        }

    }
*/

    private void captureImage() {
        Log.d(TAG, "captureImage: called");


        if (bitmap != null){
            Log.d(TAG, "onFinish: bitmap is not null");
            createImageFromBitmap(bitmap);
            eyeBlinkCount=0;
//            taskTextView.setText("Image captured");
//            goToFullImageActivity();
        }else{
            Log.d(TAG, "onFinish: bitmap is null");
        }
    }

    private void goToFullImageActivity() {
        context.startActivity(IntentFactory.returnAppMainActivity(context).putExtra("imgType","faceMatch_with_aadhaar"));
    }

    private String encodeUserImage(Bitmap bmp) {

        try {

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            BitmapHolder.getInstance().setLiveFaceImageEncoded(encoded);
            return encoded;
        } catch (Exception e) {
            e.printStackTrace();
            return "n/a";
        }

    }



    public  String createImageFromBitmap(Bitmap bitmap) {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }



    private void processFaces(List<Face> faces,Image mediaImage) {
try{
    if (faces.size() == 1) {

        Bitmap rotatedBitmap;
        Face face = faces.get(0);
        Bitmap frame_bmp = ImageConvertUtils.getInstance().getUpRightBitmap(image);
        Matrix rotationMatrix = new Matrix();
        if(frame_bmp.getWidth() >= frame_bmp.getHeight()){
            rotationMatrix.setRotate(90);
        }else{
            rotationMatrix.setRotate(0);
        }
        rotatedBitmap = Bitmap.createBitmap(frame_bmp,0,0,frame_bmp.getWidth(),frame_bmp.getHeight(),rotationMatrix,true);
        liveBitmap = Bitmap.createBitmap(frame_bmp,0,0,frame_bmp.getWidth(),frame_bmp.getHeight(),rotationMatrix,true);

        RectF boundingBox = new RectF(face.getBoundingBox());
        //Crop out bounding box from whole Bitmap(image)
        Bitmap cropped_face = getCropBitmapByCPU(rotatedBitmap, boundingBox);
//        Bitmap cropped_face = getCroppedFace(rotatedBitmap, face);
        Bitmap scaled = getResizedBitmap(cropped_face, 112, 112);
        Bitmap flipped = flip(scaled);

        if (checkLiveNessOfFace(face)){
            recognizeImage(flipped);
        }



    }
}catch (Exception exception){
    Log.d(TAG, "processFaces: unable to process faces");
}
        

    }

    private static Bitmap getCropBitmapByCPU(Bitmap source, RectF cropRectF) {
        Bitmap resultBitmap = Bitmap.createBitmap((int) cropRectF.width(),
                (int) cropRectF.height(), Bitmap.Config.ARGB_8888);
        Canvas cavas = new Canvas(resultBitmap);

        // draw background
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setColor(Color.WHITE);
        cavas.drawRect(
                new RectF(0, 0, cropRectF.width(), cropRectF.height()),
                paint);

        Matrix matrix = new Matrix();
        matrix.postTranslate(-cropRectF.left, -cropRectF.top);

        cavas.drawBitmap(source, matrix, paint);

        if (source != null && !source.isRecycled()) {
            source.recycle();
        }

        return resultBitmap;
    }


    private boolean checkLiveNessOfFace(Face face) {
        boolean isLiveImage = false;
        try{
            if (face!=null){

                    float smilingProb = face.getSmilingProbability();

                    if ( smilingProb<0.1){
                        boolean isMovementDetected = false;
//                Log.d(TAG, "checkLiveNessOfFace: mouth is visible check face rotation "+smilingProb);
                        if (face.getLeftEyeOpenProbability()<0.5){
                            isMovementDetected = true;
                        }

                        if (face.getRightEyeOpenProbability()<0.5){
                            isMovementDetected = true;
                        }

                        if (checkFaceRotation(face)){
                            isMovementDetected = true;
                        }

                        if (isMovementDetected){
                            Log.d(TAG, "checkLiveNessOfFace: movement detected person is live");
                           isLiveImage = isMovementDetected;
                        }else{
                            Log.d(TAG, "checkLiveNessOfFace: no movement detected ");
                        }
                    }


            }

        }catch (Exception exception){
            Log.d(TAG, "checkLiveNessOfFace: exception "+exception.getLocalizedMessage());
        }

        return isLiveImage;
    }


    public Bitmap flip(Bitmap src) {
        // create new matrix for transformation
         Matrix matrix = new Matrix();

        matrix.preScale(-1.0f, 1.0f);

        // return transformed image
         return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    private  Bitmap getCroppedFace(Bitmap bitmap, Face currFace) {
        Rect bounds = currFace.getBoundingBox();
        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left, bounds.top, bounds.width()+100, bounds.height()+200);
        return croppedBitmap;

    }

    private Bitmap toBitmap(Image image) {

        byte[] nv21=YUV_420_888toNV21(image);


        YuvImage yuvImage = new YuvImage(nv21, ImageFormat.NV21, image.getWidth(), image.getHeight(), null);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, yuvImage.getWidth(), yuvImage.getHeight()), 75, out);

        byte[] imageBytes = out.toByteArray();
        //System.out.println("bytes"+ Arrays.toString(imageBytes));

        //System.out.println("FORMAT"+image.getFormat());

        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

    //IMPORTANT. If conversion not done ,the toBitmap conversion does not work on some devices.
    private static byte[] YUV_420_888toNV21(Image image) {

        int width = image.getWidth();
        int height = image.getHeight();
        int ySize = width*height;
        int uvSize = width*height/4;

        byte[] nv21 = new byte[ySize + uvSize*2];

        ByteBuffer yBuffer = image.getPlanes()[0].getBuffer(); // Y
        ByteBuffer uBuffer = image.getPlanes()[1].getBuffer(); // U
        ByteBuffer vBuffer = image.getPlanes()[2].getBuffer(); // V

        int rowStride = image.getPlanes()[0].getRowStride();
        assert(image.getPlanes()[0].getPixelStride() == 1);

        int pos = 0;

        if (rowStride == width) { // likely
            yBuffer.get(nv21, 0, ySize);
            pos += ySize;
        }
        else {
            long yBufferPos = -rowStride; // not an actual position
            for (; pos<ySize; pos+=width) {
                yBufferPos += rowStride;
                yBuffer.position((int) yBufferPos);
                yBuffer.get(nv21, pos, width);
            }
        }

        rowStride = image.getPlanes()[2].getRowStride();
        int pixelStride = image.getPlanes()[2].getPixelStride();

        assert(rowStride == image.getPlanes()[1].getRowStride());
        assert(pixelStride == image.getPlanes()[1].getPixelStride());

        if (pixelStride == 2 && rowStride == width && uBuffer.get(0) == vBuffer.get(1)) {
            // maybe V an U planes overlap as per NV21, which means vBuffer[1] is alias of uBuffer[0]
            byte savePixel = vBuffer.get(1);
            try {
                vBuffer.put(1, (byte)~savePixel);
                if (uBuffer.get(0) == (byte)~savePixel) {
                    vBuffer.put(1, savePixel);
                    vBuffer.position(0);
                    uBuffer.position(0);
                    vBuffer.get(nv21, ySize, 1);
                    uBuffer.get(nv21, ySize + 1, uBuffer.remaining());

                    return nv21; // shortcut
                }
            }
            catch (ReadOnlyBufferException ex) {
                // unfortunately, we cannot check if vBuffer and uBuffer overlap
            }

            // unfortunately, the check failed. We must save U and V pixel by pixel
            vBuffer.put(1, savePixel);
        }

        // other optimizations could check if (pixelStride == 1) or (pixelStride == 2),
        // but performance gain would be less significant

        for (int row=0; row<height/2; row++) {
            for (int col=0; col<width/2; col++) {
                int vuPos = col*pixelStride + row*rowStride;
                nv21[pos++] = vBuffer.get(vuPos);
                nv21[pos++] = uBuffer.get(vuPos);
            }
        }

        return nv21;
    }


    private static Bitmap rotateBitmap(
            Bitmap bitmap, int rotationDegrees, boolean flipX, boolean flipY) {
        Matrix matrix = new Matrix();

        // Rotate the image back to straight.
        matrix.postRotate(rotationDegrees);

        // Mirror the image along the X or Y axis.
        matrix.postScale(flipX ? -1.0f : 1.0f, flipY ? -1.0f : 1.0f);
        Bitmap rotatedBitmap =
                Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        // Recycle the old bitmap if it has changed.
        if (rotatedBitmap != bitmap) {
            bitmap.recycle();
        }
        return rotatedBitmap;
    }

    private void checkFaceMask(Face face) {
        if (face!=null){
            FaceLandmark leftMouth = face.getLandmark(FaceLandmark.MOUTH_LEFT);
            FaceLandmark rightMouth = face.getLandmark(FaceLandmark.MOUTH_RIGHT);
            FaceLandmark bottomMouth = face.getLandmark(FaceLandmark.MOUTH_BOTTOM);
            FaceLandmark nose = face.getLandmark(FaceLandmark.NOSE_BASE);
            if (nose!=null){
                Log.d(TAG, "checkFaceMask: nose "+nose.getPosition()+" "+nose.getLandmarkType());

            }

            if (leftMouth!=null){
                PointF lmp = leftMouth.getPosition();
                Log.d(TAG, "checkFaceMask: mouth "+lmp);
            }
            Log.d(TAG, "checkFaceMask: smiling prob "+face.getSmilingProbability());

//            List<PointF> upperLipBottomContour =
//                    face.getContour(FaceContour.UPPER_LIP_BOTTOM);
//            List<PointF> lowerLipBottomContour =
//                    face.getContour(FaceContour.LOWER_LIP_BOTTOM).getPoints();
//            List<PointF> upperLipTopContour =
//                    face.getContour(FaceContour.UPPER_LIP_TOP).getPoints();
//            List<PointF> lowerLipTopContour =
//                    face.getContour(FaceContour.LOWER_LIP_TOP).getPoints();

//            Log.d(TAG, "checkFaceMask: "+upperLipTopContour.size() +" "+upperLipBottomContour.size() +" "+lowerLipBottomContour.size()+" "+lowerLipTopContour.size());
//            Log.d(TAG, "checkFaceMask: ");


        }
    }

    public void recognizeImage(final Bitmap bitmap) {

        if (FaceDetectionActivity.isCaptureBtnClicked){
            Log.d(TAG, "recognizeImage: capture btn clicked ");
            /*Bitmap faceBitmap = ImageConvertUtils.getInstance().getUpRightBitmap(image);

            String encodeB = encodeUserImage();
            FaceMatchModelISUeKyc.getInstance().setFaceImageEncoded(encodeB);*/
            getImageFromInputImage();
        }else{
            ByteBuffer imgData = ByteBuffer.allocateDirect(1 * inputSize * inputSize * 3 * 4);
            imgData.order(ByteOrder.nativeOrder());
            intValues = new int[inputSize * inputSize];
            //get pixel values from Bitmap to normalize
            bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
            imgData.rewind();
            for (int i = 0; i < inputSize; ++i) {
                for (int j = 0; j < inputSize; ++j) {
                    int pixelValue = intValues[i * inputSize + j];
                    if (isModelQuantized) {
                        // Quantized model
                        imgData.put((byte) ((pixelValue >> 16) & 0xFF));
                        imgData.put((byte) ((pixelValue >> 8) & 0xFF));
                        imgData.put((byte) (pixelValue & 0xFF));
                    } else { // Float model
                        imgData.putFloat((((pixelValue >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                        imgData.putFloat((((pixelValue >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                        imgData.putFloat(((pixelValue & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                    }
                }
            }
            //imgData is input to our model
            Object[] inputArray = {imgData};
            Map<Integer, Object> outputMap = new HashMap<>();
            embeedings = new float[1][OUTPUT_SIZE]; //output of model will be stored in this variable
            outputMap.put(0, embeedings);
            try {
                tfLite.runForMultipleInputsOutputs(inputArray, outputMap); //Run model
                CurrentRecognition.Recognition.getInstance().initClassifier("0", "", -1f).setExtra(embeedings);
                registered.put("new",CurrentRecognition.Recognition.getInstance());
                Object objCurrent = CurrentRecognition.Recognition.getInstance().getExtra();
                Object objCaptured = CapturedRecognition.Recognition.getInstance().getExtra();
                float distance_local = Float.MAX_VALUE;
                String id = "0";
                String label = "?";
                if (registered.size() > 0) {
                    capturedEmb = (float[][]) CapturedRecognition.Recognition.getInstance().getExtra();
                    final Pair<String, Float> nearest = findMatchingFace(capturedEmb[0]);//Find 2 closest matching face
                    if (nearest!= null) {
                        final String name = nearest.first; //get name and distance of closest matching face
                        // label = name;
                        distance_local = nearest.second;
                        if (developerMode)
                        {
                            if(distance_local<distance){//If distance between Closest found face is more than 1.000 ,then output UNKNOWN face.
                                textView.setText("Nearest: "+name +"\nDist: "+ String.format("%.3f",distance_local)+"\n2nd Nearest: "+nearest.first +"\nDist: "+ String.format("%.3f",nearest.second));
                            }
                            else{
                                textView.setText("Unknown "+"\nDist: "+String.format("%.3f",distance_local)+"\nNearest: "+name +"\nDist: "+ String.format("%.3f",distance_local)+"\n2nd Nearest: "+nearest.first +"\nDist: "+ String.format("%.3f",nearest.second));
                            }
                        }
                        else
                        {
                            if(distance_local<distance) //If distance between Closest found face is more than 1.000 ,then output UNKNOWN face.
                            {
                                faceMatchListener.faceMatchingStatus(true);
                                captureText.setText("Capture Now");
                                imgDone.setVisibility(View.VISIBLE);
                            }
                            else{
                                faceMatchListener.faceMatchingStatus(false);
                                captureText.setText("Place your face within circle");
                                imgDone.setVisibility(View.GONE);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private MappedByteBuffer loadModelFile(Activity activity, String MODEL_FILE) throws IOException {
        AssetFileDescriptor fileDescriptor = activity.getAssets().openFd(MODEL_FILE);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    //Compare Faces by distance between face embeddings
    private Pair<String, Float> findMatchingFace(float[] emb) {
        Pair<String, Float> ret = null;
        for (Map.Entry<String, CurrentRecognition.Recognition> entry : registered.entrySet()) {
            final String name = entry.getKey();
            final float[] knownEmb = ((float[][]) entry.getValue().getExtra())[0];

            float distance = 0;
            for (int i = 0; i < emb.length; i++) {
                float diff = emb[i] - knownEmb[i];
                distance += diff*diff;
            }
            distance = (float) Math.sqrt(distance);
            if (ret == null || distance < ret.second) {
                ret = new Pair<>(name, distance);
            }
        }

        return ret;

    }


    @Override
    public void faceMatchingStatus(boolean isMatching) {
        if (isMatching){
            Toast.makeText(context, "Yulu", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, " No Yulu", Toast.LENGTH_SHORT).show();

        }
    }
    private void getImageFromInputImage(){
        // Pass image to an ML Kit Vision API
        try {
            Log.d(TAG, "analyze: getting bitmap");
/*
            bitmap = ImageConvertUtils.getInstance().getUpRightBitmap(image);
*/
            String encodeB = encodeUserImage(liveBitmap);
            Log.d(TAG, "getImageFromInputImage: liveBitmap :: "+liveBitmap +" encodeB :: "+encodeB);
            FaceMatchModelISUeKyc.getInstance().setFaceImageEncoded(encodeB);
            FaceDetectionActivity.isCaptureBtnClicked = false;

//            captureImage();
            goToFullImageActivity();

        } catch (Exception e) {
            Log.d(TAG, "getImageFromInputImage: exception :: "+e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

}
