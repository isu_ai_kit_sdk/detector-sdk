package com.iserveu.isuekyc.face_detection.model;

import android.content.Context;

import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.support.image.ImageProcessor;

public class FaceNetModel {

    private Context context;
    private ModelInfo model;
    private Boolean useGpu;
    private Boolean useXNNPack;
    private int imgSize ;
    private int embeddingDim ;
    private Interpreter interpreter;
    private ImageProcessor imageTensorProcessor;
    public FaceNetModel(Context context, ModelInfo model, Boolean useGpu, Boolean useXNNPack) {
        this.context = context;
        this.model = model;
        this.useGpu = useGpu;
        this.useXNNPack = useXNNPack;
        this.imgSize = model.getInputDims();
        this.embeddingDim = model.getOutputDims();
        initImageProcessor();

    }

    private void initImageProcessor() {
                
    }


}
