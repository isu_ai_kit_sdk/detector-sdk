package com.iserveu.isuekyc.face_detection;

public interface FaceMatchListener {
    void faceMatchingStatus(boolean isMatching);
}
