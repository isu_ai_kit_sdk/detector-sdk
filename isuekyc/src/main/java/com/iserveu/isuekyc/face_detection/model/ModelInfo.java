package com.iserveu.isuekyc.face_detection.model;

public class ModelInfo {

    private String name ;
    private String assetsFilename;
    private float cosineThreshold;
    private float l2Threshold;
    private int outputDims;
    private int inputDims;


    public ModelInfo(String name, String assetsFilename, float cosineThreshold, float l2Threshold, int outputDims, int inputDims) {
        this.name = name;
        this.assetsFilename = assetsFilename;
        this.cosineThreshold = cosineThreshold;
        this.l2Threshold = l2Threshold;
        this.outputDims = outputDims;
        this.inputDims = inputDims;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssetsFilename() {
        return assetsFilename;
    }

    public void setAssetsFilename(String assetsFilename) {
        this.assetsFilename = assetsFilename;
    }

    public float getCosineThreshold() {
        return cosineThreshold;
    }

    public void setCosineThreshold(float cosineThreshold) {
        this.cosineThreshold = cosineThreshold;
    }

    public float getL2Threshold() {
        return l2Threshold;
    }

    public void setL2Threshold(float l2Threshold) {
        this.l2Threshold = l2Threshold;
    }

    public int getOutputDims() {
        return outputDims;
    }

    public void setOutputDims(int outputDims) {
        this.outputDims = outputDims;
    }

    public int getInputDims() {
        return inputDims;
    }

    public void setInputDims(int inputDims) {
        this.inputDims = inputDims;
    }
}
