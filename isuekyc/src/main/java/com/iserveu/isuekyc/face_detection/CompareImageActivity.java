package com.iserveu.isuekyc.face_detection;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.databinding.ActivityCompareImageBinding;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

public class CompareImageActivity extends AppCompatActivity {

    private static final String TAG = CompareImageActivity.class.getSimpleName();
    private ActivityCompareImageBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =ActivityCompareImageBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        showLiveImage();
        showCapturedImage();


    }

    private void showLiveImage() {
        try {
//            Bitmap bitmap = decodeBitmap(BitmapHolder.getInstance().getLiveFaceImageEncoded());
            Bitmap bitmap = BitmapHolder.getInstance().getLiveImageBitmap();
            binding.newImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "showLiveImage: "+e.getLocalizedMessage());
        }
    }

    private void showCapturedImage() {
        try {

            Bitmap bitmap = decodeBitmap(BitmapHolder.getInstance().getAadhaarFrontImageEncoded());
            binding.oldImage.setImageBitmap(bitmap);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "showCapturedImage: "+e.getLocalizedMessage());

        }

    }

    private Bitmap decodeBitmap(String encodedImage) {

        byte[] decodedBytes = Base64.decode(encodedImage, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }
    private Bitmap getSavedBitmap(String s) {

        Bitmap bmp = null;
        try {
            FileInputStream is = this.openFileInput(s);
            bmp = BitmapFactory.decodeStream(is);

            is.close();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            /*byte[] byteArray = byteArrayOutputStream .toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);*/
            return bmp;
        } catch (Exception e) {
            e.printStackTrace();
            return bmp;
        }

    }
}