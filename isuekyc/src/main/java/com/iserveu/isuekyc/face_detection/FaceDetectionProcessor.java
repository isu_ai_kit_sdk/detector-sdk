package com.iserveu.isuekyc.face_detection;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.google.mlkit.vision.face.FaceLandmark;
import com.iserveu.isuekyc.utils.Constants;
import com.iserveu.isuekyc.utils.FullImageView;

import java.io.FileOutputStream;
import java.util.List;

public class FaceDetectionProcessor {
    private static final String TAG = FaceDetectionProcessor.class.getSimpleName();
    private Context context;
    private static FaceDetectionProcessor faceDetectionProcessor;
    private Face face;
    private Bitmap imageBitmap;

    public static FaceDetectionProcessor getInstance(){
        if (faceDetectionProcessor == null){
            faceDetectionProcessor = new FaceDetectionProcessor();
        }
        return faceDetectionProcessor;
    }

    public FaceDetectionProcessor detectFaces(Context mContext,Bitmap mBitmap){
        context = mContext;
        InputImage image = InputImage.fromBitmap(mBitmap,0);
        FaceDetectorOptions options =
                new FaceDetectorOptions.Builder()
                        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
//                        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                        .setMinFaceSize(0.15f)
                        .enableTracking()
                        .build();

        FaceDetector detector = FaceDetection.getClient(options);
        detector.process(image).addOnSuccessListener(new OnSuccessListener<List<Face>>() {
            @Override
            public void onSuccess(List<Face> faces) {
                if (faces!=null){
                    if (faces.size()==1){
                        face = faces.get(0);
                        imageBitmap = mBitmap;
                    }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnCompleteListener(new OnCompleteListener<List<Face>>() {
            @Override
            public void onComplete(@NonNull Task<List<Face>> task) {

            }
        });
        return faceDetectionProcessor;
    }

    public Bitmap getCroppedFace() {
        Rect bounds = face.getBoundingBox();
        Bitmap croppedBitmap = Bitmap.createBitmap(imageBitmap,bounds.left, bounds.top, bounds.width(), bounds.height());
        return croppedBitmap;

    }




    public void detectFace(Context mContext,Bitmap mBitmap){
        context = mContext;
        InputImage image = InputImage.fromBitmap(mBitmap,0);
        FaceDetectorOptions options =
                new FaceDetectorOptions.Builder()
                        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                        .setMinFaceSize(0.15f)
                        .enableTracking()
                        .build();

        FaceDetector detector = FaceDetection.getClient(options);
        detector.process(image).addOnSuccessListener(new OnSuccessListener<List<Face>>() {
            @Override
            public void onSuccess(List<Face> faces) {
                if (faces!=null){
                    processFaces(faces,mBitmap);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnCompleteListener(new OnCompleteListener<List<Face>>() {
            @Override
            public void onComplete(@NonNull Task<List<Face>> task) {

            }
        });

    }

    private void processFaces(List<Face> faces,Bitmap  bitmap) {
        if (faces.size()==1){
//            Toast.makeText(context, "1 face detected", Toast.LENGTH_SHORT).show();
            Face currFace =faces.get(0);
                Integer trackingId =  currFace.getTrackingId();
//            saveDetectedFace(currFace,bitmap);
            getFaceLandmarks(face);
        }else  if (faces.size()>1){
            Toast.makeText(context, "Multiple faces detected", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, "No faces detected", Toast.LENGTH_SHORT).show();
        }
    }

    private void getFaceLandmarks(Face face) {
        if (face!=null){
            Log.d(TAG, "getFaceLandmarks: "+face.getSmilingProbability());
            Log.d(TAG, "getFaceLandmarks: "+face.getSmilingProbability());



        FaceLandmark leftEar = face.getLandmark(FaceLandmark.LEFT_EAR);
        if (leftEar != null) {
            PointF leftEarPos = leftEar.getPosition();
        }else {
            Log.d(TAG, "getFaceLandmarks: leftEar is null");
        }
        FaceLandmark leftEye = face.getLandmark(FaceLandmark.LEFT_EYE);
        if (leftEye != null) {
            PointF leftEyePos = leftEye.getPosition();
        }else {
            Log.d(TAG, "getFaceLandmarks: leftEye is null");

        }
        FaceLandmark leftMouth = face.getLandmark(FaceLandmark.MOUTH_LEFT);
        if (leftMouth != null) {
            PointF leftMouthPos = leftMouth.getPosition();
        }else {
            Log.d(TAG, "getFaceLandmarks: leftMouth is null");

        }
        }
    }

    private void saveDetectedFace(Face currFace,Bitmap bitmap) {
        Rect bounds = currFace.getBoundingBox();
        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left-50, bounds.top-150, bounds.width()+100, bounds.height()+200);
        try {
            //Write file
            String filename = "detected_bitmap.png";
            FileOutputStream stream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            //Cleanup
            stream.close();
            croppedBitmap.recycle();

            //Pop intent
            Intent in1 = new Intent(context, FullImageView.class);
            in1.putExtra(Constants.DETECTED_FACE_FROM_DOCUMENT, filename);
            context.startActivity(in1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showDetectedFace(Bitmap croppedBitmap) {

    }

}
