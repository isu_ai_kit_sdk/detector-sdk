package com.example.isuekyc;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.isuekyc.databinding.ActivityMainBinding;
import com.iserveu.isuekyc.aadhaar_back.AadhaarBackModelISUeKyc;
import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontModelISUeKyc;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.face_detection.FaceMatchModelISUeKyc;
import com.iserveu.isuekyc.intent.IntentFactory;
import com.iserveu.isuekyc.liveness.FaceLivenessModelISUeKyc;
import com.iserveu.isuekyc.pan_card.PanCardModelISUeKyc;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ActivityMainBinding binding;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        onClickListeners();
        showResponseDialog();
    }

    private void showResponseDialog() {
        // create dialog
        LayoutInflater inflater= LayoutInflater.from(this);
        View view=inflater.inflate(R.layout.custom_dialog, null);

        TextView textview=(TextView)view.findViewById(R.id.json_text_data);
        ImageView imageView1 = view.findViewById(R.id.image1);
        ImageView imageView2 =view.findViewById(R.id.image2);
        Button closeButton = view.findViewById(R.id.close_dialog_button);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Response ");
        alertDialog.setView(view);



        // get response
        if (getIntent().hasExtra("imgType")){
            JSONObject jsonObject = new JSONObject();

            String imgType = getIntent().getStringExtra("imgType");

            /**  PAN CARD  **/
            if (imgType.equalsIgnoreCase("panCard")){
                String  panName= PanCardModelISUeKyc.getInstance().getPanName();
                String panDOB = PanCardModelISUeKyc.getInstance().getPanBOB();
                String panFatherName = PanCardModelISUeKyc.getInstance().getPanFatherName();
                String panNo = PanCardModelISUeKyc.getInstance().getPanNo();
                String panMotherName = PanCardModelISUeKyc.getInstance().getPanMotherName();
                String encodedUserSignature = PanCardModelISUeKyc.getInstance().getPanEncodedSignature();
                String encodedUserImage = PanCardModelISUeKyc.getInstance().getPanEncodedUserImage();
                boolean isMotherName = PanCardModelISUeKyc.getInstance().isMotherNameFound();

                try {
                    jsonObject.put("panName",panName);
                    jsonObject.put("panFatherName",panFatherName);
                    jsonObject.put("isMotherName",isMotherName);
                    jsonObject.put("panMotherName",panMotherName);
                    jsonObject.put("panDOB",panDOB);
                    jsonObject.put("panNo",panNo);



                } catch (JSONException e) {
                    Log.d(TAG, "showResponseDialog: exception :: "+e.getLocalizedMessage());
                    e.printStackTrace();
                }

                textview.setText(jsonObject.toString());
                try {
                    Bitmap userBitmap = decodeBitmap(encodedUserImage);
                    imageView1.setImageBitmap(userBitmap);
                }catch (Exception exception){
                    Log.d(TAG, "showResponseDialog: "+exception.getLocalizedMessage());
                }

                try {
                    Bitmap userSignBitmap = decodeBitmap(encodedUserSignature);
                    imageView2.setImageBitmap(userSignBitmap);
                }catch (Exception exception){
                    Log.d(TAG, "showResponseDialog: "+exception.getLocalizedMessage());
                }
            }


            /**  AADHAAR FRONT  **/

            if (imgType.equalsIgnoreCase("aadharFront")){

                String name = AadhaarFrontModelISUeKyc.getInstance().getAadhaarName();
                String dob = AadhaarFrontModelISUeKyc.getInstance().getAadhaarDOB();
                String gender = AadhaarFrontModelISUeKyc.getInstance().getAadhaarGender();
                String aadharNo = AadhaarFrontModelISUeKyc.getInstance().getAadhaarNumber();
                String vid = AadhaarFrontModelISUeKyc.getInstance().getAadhaarVIDNumber();
                String encodedUserImage = AadhaarFrontModelISUeKyc.getInstance().getAadhaarEncodedUserImage();
                String userfrontImage = AadhaarFrontModelISUeKyc.getInstance().getFrontImageUrl();

                    try {
                        jsonObject.put("name", name);
                        jsonObject.put("dob",dob);
                        jsonObject.put("gender",gender);
                        jsonObject.put("aadharNo",aadharNo);
                        jsonObject.put("vid",vid);

                        Log.d(TAG, "run: aadharFrontResponseJSON "+jsonObject.toString());
                        textview.setText(jsonObject.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "onCreate: encodedUserImageAadhaar :: "+e.getLocalizedMessage());
                    }


                    try {
                        Bitmap imgBitmap = decodeBitmap(encodedUserImage);
                        imageView1.setImageBitmap(imgBitmap);
                    }catch (Exception exception){
                        Log.d(TAG, "showResponseDialog: exception :: "+exception.getLocalizedMessage());
                    }


            }



            /**  AADHAAR BACK  **/

            if (imgType.equalsIgnoreCase("aadharBack")){
                String address = AadhaarBackModelISUeKyc.getInstance().getAadhaarAddress();
                String state = AadhaarBackModelISUeKyc.getInstance().getAadhaarState();
                String pin = AadhaarBackModelISUeKyc.getInstance().getAadhaarPIN();
                String aadharNo = AadhaarBackModelISUeKyc.getInstance().getAadhaarNumber();
                String vid = AadhaarBackModelISUeKyc.getInstance().getAadhaarVIDNumber();

                try {
                    jsonObject.put("address",address);
                    jsonObject.put("state",state);
                    jsonObject.put("pin",pin);
                    jsonObject.put("aadharNo",aadharNo);
                    jsonObject.put("vid",vid);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                textview.setText(jsonObject.toString());
            }


            /**  FACE MATCH WITH AADHAAR  **/

            if (imgType.equalsIgnoreCase("faceMatch_with_aadhaar")){
                try {
                    String encoded = FaceMatchModelISUeKyc.getInstance().getFaceImageEncoded();;
                    Bitmap bitmap = decodeBitmap(encoded);
                    imageView2.setImageBitmap(bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "showResponseDialog: "+e.getLocalizedMessage());
                }

                try {
                    String encoded = AadhaarFrontModelISUeKyc.getInstance().getAadhaarEncodedUserImage();
                    Bitmap bitmap = decodeBitmap(encoded);
                    imageView1.setImageBitmap(bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "showResponseDialog: "+e.getLocalizedMessage());
                }

            }



            /**  FACE LIVENESS  **/

            if (imgType.equalsIgnoreCase("liveness_image")){
                try {
                    String encImage = FaceLivenessModelISUeKyc.getInstance().getFaceLivenessImageEncoded();
                    if (encImage!=null){
                        Bitmap imgBitmap = decodeBitmap(encImage);
                        imageView1.setImageBitmap(imgBitmap);
                    }
                }catch (Exception exception){
                    Log.d(TAG, "showResponseDialog: exception "+exception.getLocalizedMessage());
                }

            }




            // show dialog
            AlertDialog alert = alertDialog.create();
            alert.show();

            closeButton.setOnClickListener(view1 -> {
                if (alert!=null){
                    alert.dismiss();
                }
            });
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    alert.dismiss();
                }
            });

        }

    }


    private void onClickListeners() {
        binding.aadharFrontBtnHyper.setOnClickListener(view -> {
            Activity activity = getActivityFromContext(this);
            IntentFactory.responseActivityName = activity;
            IntentFactory.responsePackageName = activity.getLocalClassName();
            Log.d(TAG, "onClickListeners: responsePackageName :: "+IntentFactory.responsePackageName);
            startActivity(IntentFactory.returnAadhaarFrontHVCameraActivity(this));
        });
        binding.aadharLivenessBtnHyper.setOnClickListener(view ->{
            Activity activity = getActivityFromContext(this);
            IntentFactory.responseActivityName = activity;
            IntentFactory.responsePackageName = activity.getLocalClassName();
            Log.d(TAG, "onClickListeners: responsePackageName :: "+IntentFactory.responsePackageName);
            startActivity(IntentFactory.returnAadhaarFaceHVCameraActivity(this));

        });
        binding.panCardBtn.setOnClickListener(view -> {
            Activity activity = getActivityFromContext(this);
            IntentFactory.responseActivityName = activity;
            IntentFactory.responsePackageName = activity.getLocalClassName();
            Log.d(TAG, "onClickListeners: responsePackageName :: "+IntentFactory.responsePackageName);
            startActivity(IntentFactory.returnPanCameraActivity(this));
             });
        binding.aadharFrontBtn.setOnClickListener(view -> {
            startActivity(IntentFactory.returnAadhaarFrontCameraActivity(this));
        });
        binding.aadharBackBtn.setOnClickListener(view -> {
            startActivity(IntentFactory.returnAadhaarBackCameraActivity(this));
        });
        binding.faceMatchBtn.setOnClickListener(view -> {
            if (BitmapHolder.getInstance().getAadhaarFaceImageEncoded()!=null){
                startActivity(IntentFactory.returnFaceMatchActivity(this));
            }else{
                Toast.makeText(this, "Please upload aadhaar card", Toast.LENGTH_SHORT).show();
            }
        });
        binding.livenessBtn.setOnClickListener(view -> {
            startActivity(IntentFactory.returnLivenessFaceDetectionActivity(this));
        });
    }

    private Bitmap decodeBitmap(String encodedImage) {

        byte[] decodedBytes = Base64.decode(encodedImage, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }




    private static Activity getActivityFromContext(Context context)
    {
        if (context == null)
        {
            return null;
        }
        else if (context instanceof ContextWrapper)
        {
            if (context instanceof Activity)
            {
                return (Activity) context;
            }
            else
            {
                return getActivityFromContext(((ContextWrapper) context).getBaseContext());
            }
        }
        return null;
    }
}